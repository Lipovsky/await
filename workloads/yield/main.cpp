#include <await/tasks/exe/fibers/pool.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/submit.hpp>

#include <await/await.hpp>

#include <wheels/core/stop_watch.hpp>

#include <iostream>

using namespace await;

int main() {
  tasks::fibers::Pool pool{/*threads=*/1};

  while (true) {
    wheels::StopWatch stop_watch;

    auto f = futures::Submit(pool, []() {
      for (size_t i = 0; i < 1'000'000; ++i) {
        fibers::YieldFuture();
      }
    });

    Await(std::move(f));

    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(stop_watch.Elapsed());

    std::cout << ms.count() << std::endl;
  }

  pool.Stop();

  return 0;
}
