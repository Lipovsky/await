#pragma once

#include <await/futures/types/future.hpp>

#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/atomic.hpp>

#include <wheels/core/unit.hpp>

namespace tutorial {

// 3 threads are involved:
// - consumer:
//      Await(Waiter) -> Waiter::Start(IConsumer*) -> AsyncEvent::Subscribe(Waiter*)
// - producer:
//      AsyncEvent::Fire()
// - interrupter
//      Waiter::Forward(Signal) -> AsyncEvent::Cancel

class AsyncEvent {
  enum class Status : int {
    Fire = 1,
    Cancel = 2,
  };

  class Waiter : private await::cancel::HandlerBase {
   public:
    using ValueType = wheels::Unit;

   private:
    using IUnitConsumer = await::futures::IConsumer<wheels::Unit>;

   public:
    explicit Waiter(AsyncEvent* event)
        : event_(event) {
    }

    // Non-copyable
    Waiter(const Waiter&) = delete;

    // Movable
    Waiter(Waiter&& that) {
      event_ = std::exchange(that.event_, nullptr);
    }

    // Thread: consumer
    void Start(IUnitConsumer* consumer) {
      consumer_ = consumer;

      // NB: Before event_->Subscribe(Waiter*)
      // to order Subscribe and Unsubscribe on cancel token
      CancelToken().Subscribe(this);

      event_->Subscribe(this);
    }

    // Invoked exactly once by producer, consumer or interrupter
    void Resume(Status s) {
      status_ = s;
      // NB: Unsubscribe and Forward can race
      CancelToken().Unsubscribe(this);

      // Resume or Forward
      if (last_.exchange(true)) {
        CompleteFuture();
      }
    }

   private:
    // cancel::IHandler

    // Thread: interrupter
    // Invoked exactly once
    void Forward(await::cancel::Signal s) override {
      if (s.CancelRequested()) {
        event_->Cancel();
      }

      // Resume or Forward
      if (last_.exchange(true)) {
        CompleteFuture();
      }
    }

    void CompleteFuture() {
      if (status_ == Status::Fire) {
        consumer_->Consume(wheels::Unit{});
      } else {
        consumer_->Cancel({});
      }
    }

    await::cancel::Token CancelToken() {
      return consumer_->CancelToken();
    }

   private:
    AsyncEvent* event_;
    IUnitConsumer* consumer_;

    Status status_{0};
    // Resume or Forward
    twist::ed::stdlike::atomic<bool> last_{false};
  };

 public:
  using Future = Waiter;

  // Asynchronous
  Future Wait() {
    return Waiter{this};
  }

  // Thread: producer
  void Fire() {
    Locker locker(mutex_);

    if (waiter_ != nullptr) {
      // Rendezvous: Producer + Consumer
      Waiter* waiter = std::exchange(waiter_, nullptr);
      locker.unlock();
      waiter->Resume(Status::Fire);
    } else {
      fired_ = true;
    }
  }

 private:
  using Mutex = twist::ed::stdlike::mutex;
  using Locker = std::unique_lock<Mutex>;

 private:
  void Cancel() {
    Locker locker(mutex_);

    if (waiter_ != nullptr) {
      // Rendezvous: Consumer + Interrupter
      Waiter* waiter = std::exchange(waiter_, nullptr);
      locker.unlock();
      waiter->Resume(Status::Cancel);
    } else {
      canceled_ = true;
    }
  }

  void Subscribe(Waiter* waiter) {
    Locker locker(mutex_);

    if (canceled_) {
      // Rendezvous: Consumer + Interrupter
      locker.unlock();
      waiter->Resume(Status::Cancel);
    } else if (fired_) {
      // Rendezvous: Consumer + Producer
      locker.unlock();
      waiter->Resume(Status::Fire);
    } else {
      waiter_ = waiter;
    }
  }

 private:
  Mutex mutex_;

  bool fired_ = false;  // Producer
  bool canceled_ = false;  // Interrupter
  Waiter* waiter_ = nullptr;  // Consumer
};

}  // namespace tutorial
