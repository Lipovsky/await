include(FetchContent)

ProjectLog("Fetch dependencies")

# --------------------------------------------------------------------

# set(FETCHCONTENT_FULLY_DISCONNECTED ON)
# set(FETCHCONTENT_QUIET OFF)

# --------------------------------------------------------------------

# Formatting

FetchContent_Declare(
        fmt
        GIT_REPOSITORY https://github.com/fmtlib/fmt.git
        GIT_TAG master
)
FetchContent_MakeAvailable(fmt)

# --------------------------------------------------------------------

# Core utilities

FetchContent_Declare(
        wheels
        GIT_REPOSITORY https://gitlab.com/Lipovsky/wheels.git
        GIT_TAG master
)
FetchContent_MakeAvailable(wheels)

# --------------------------------------------------------------------

# Context switching

FetchContent_Declare(
        sure
        GIT_REPOSITORY https://gitlab.com/Lipovsky/sure.git
        GIT_TAG master
)
FetchContent_MakeAvailable(sure)

# --------------------------------------------------------------------

# Fault injection

FetchContent_Declare(
        twist
        GIT_REPOSITORY https://gitlab.com/Lipovsky/twist.git
        GIT_TAG 831f73060fff8c596d50942a1b920c1215c0b220
)
FetchContent_MakeAvailable(twist)

# --------------------------------------------------------------------

# Intrusive reference count

FetchContent_Declare(
        refer
        GIT_REPOSITORY https://gitlab.com/Lipovsky/refer.git
        GIT_TAG master
)
FetchContent_MakeAvailable(refer)

# --------------------------------------------------------------------

# Context

FetchContent_Declare(
        carry
        GIT_REPOSITORY https://gitlab.com/Lipovsky/carry.git
        GIT_TAG master
)
FetchContent_MakeAvailable(carry)

# --------------------------------------------------------------------

# expected

FetchContent_Declare(
        expected
        GIT_REPOSITORY https://github.com/TartanLlama/expected.git
        GIT_TAG cmake_love
)
FetchContent_MakeAvailable(expected)

# --------------------------------------------------------------------

# Memory allocation

if(AWAIT_MIMALLOC)

FetchContent_Declare(
        mimalloc
        GIT_REPOSITORY https://github.com/microsoft/mimalloc
        GIT_TAG master
)
FetchContent_MakeAvailable(mimalloc)

endif()
