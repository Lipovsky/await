#pragma once

#include <cstdint>

namespace await::cancel {

namespace detail {

//////////////////////////////////////////////////////////////////////

// Integer value or pointer packed into single 64-bit value

class StateVariant {
 public:
  // Values

  bool IsValue() const {
    return state_ & 1;
  }

  uint32_t AsValue() const {
    return state_ >> 1;
  }

  static StateVariant Value(uint32_t value) {
    return {1 | (value << 1)};
  }

  // Pointer

  bool IsPointer() const {
    return !IsValue();
  }

  template <typename T>
  T* AsPointerTo() {
    return reinterpret_cast<T*>(state_);
  }

  template <typename T>
  static StateVariant Pointer(T* ptr) {
    return {reinterpret_cast<uintptr_t>(ptr)};
  }

  // Copyable

  StateVariant(const StateVariant&) = default;
  StateVariant& operator=(const StateVariant&) = default;

  // Comparable

  bool operator==(const StateVariant& that) const {
    return state_ == that.state_;
  }

  bool operator!=(const StateVariant& that) const {
    return state_ != that.state_;
  }

 private:
  StateVariant(uintptr_t state)
      : state_(state) {
  }

 private:
  uintptr_t state_;
};

}  // namespace detail

}  // namespace await::cancel
