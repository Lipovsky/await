#pragma once

#include <await/cancel/detail/state.hpp>

#include <await/tasks/core/executor.hpp>

#include <await/thread/this.hpp>

#include <refer/ref_counted.hpp>

#include <wheels/core/defer.hpp>

namespace await::cancel {

namespace detail {

// [Await.Task(Shared,User)]
template <typename F>
class Handler final : public HandlerBase,
                      public tasks::TaskBase,
                      public refer::RefCounted<Handler<F>> {
  using Self = Handler<F>;

 public:
  Handler(F handler, tasks::IExecutor& e)
      : handler_(std::move(handler)),
        executor_(e) {
  }

  // IHandler

  // Release-reference operation
  void Forward(Signal signal) override {
    if (signal.CancelRequested()) {
      ExecuteHandler();
    } else {
      Self::ReleaseRef();
    }
  }

  // ITask

  void Run() noexcept override {
    RunHandler();

    // Memory management:
    Self::ReleaseRef();
  }

 private:
  void SetupContext(thread::IThread* carrier) {
    carrier->SetExecutor(&executor_);
    // TODO
    // carrier->SetUserContext(user_context_);
  }

  void RunHandler() {
    auto* carrier = thread::This();

    SetupContext(carrier);

    wheels::Defer cleanup([carrier]() {
      carrier->ResetContext();
    });

    // Run user code
    handler_();
  }

 private:
  void ExecuteHandler() {
    executor_.Submit(this, tasks::SchedulerHint::UpToYou);
  }

 private:
  F handler_;

  tasks::IExecutor& executor_;
};

}  // namespace detail

}  // namespace await::cancel
