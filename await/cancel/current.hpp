#pragma once

#include <await/cancel/token.hpp>

namespace await::cancel {

// Cancel token of the current task (attached to carrier)
Token Current();

}  // namespace await::cancel
