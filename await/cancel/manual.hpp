#pragma once

#include <await/cancel/token.hpp>

#include <await/cancel/states/strand.hpp>
#include <refer/manual.hpp>

namespace await::cancel {

template <typename StateImpl = detail::StrandStateBase>
class ManualState {
 private:
  // Cancellation state
  class Impl : public StateImpl,
               public refer::Unmanaged {
    //
  };

 public:
  ManualState() = default;

  // Non-copyable
  ManualState(const ManualState&) = delete;
  ManualState& operator=(const ManualState&) = delete;

  ManualState(ManualState&& that) {
    WHEELS_ASSERT(!that.IsAllocated(), "Cannot move allocated inlined state");
    // No-op
  }

  void Allocate() {
    impl_.Allocate();
  }

  // Precondition: IsAllocated() == true
  Token MakeToken() {
    return Token::FromState(impl_.Get().Get());
  }

  Source AsSource() {
    return Source::FromState(impl_.Get());
  }

  bool IsAllocated() const {
    return impl_.Allocated();
  }

 private:
  refer::ManualLifetime<Impl> impl_;
};

}  // namespace await::cancel
