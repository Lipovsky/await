#pragma once

#include <await/cancel/detail/state.hpp>

#include <await/cancel/detail/state_variant.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace await::cancel {

namespace detail {

//////////////////////////////////////////////////////////////////////

// Cancel state for strand of tasks
// Progress guarantee: Wait-freedom

// Single-producer + single-consumer

class StrandStateBase : public StateBase {
  static const uint32_t kInitial = 0;
  static const uint32_t kCancelRequested = 1;

  using State = StateVariant;

 public:
  StrandStateBase()
      : state_(State::Value(kInitial)) {
  }

  ~StrandStateBase() {
    ResetHandler(/*expected=*/nullptr);
  }

  // IHandler

  // Release-reference operation
  void Forward(Signal) override;

  // StateBase

  bool Cancellable() const override {
    return true;
  }

  // Single consumer

  // Wait-free
  bool AddHandler(HandlerBase* handler) override {
    return SetHandler(handler);
  }

  // Wait-free
  void RemoveHandler(HandlerBase* expected) override {
    ResetHandler(expected);
  }

  bool HasHandlers() const override;

  // Wait-free
  bool CancelRequested() const override;

  // Single producer

  // Wait-free
  void RequestCancel() override;

 protected:
  bool SetHandler(HandlerBase* handler);

  void ResetHandler(HandlerBase* expected);
  void ResetHandler();

 private:
  // Helpers

  static bool IsCancelRequested(State state) {
    return state.IsValue() && state.AsValue() == kCancelRequested;
  }

  static bool IsInitial(State state) {
    return state.IsValue() && state.AsValue() == kInitial;
  }

 private:
  twist::ed::stdlike::atomic<State> state_;
};

}  // namespace detail

}  // namespace await::cancel
