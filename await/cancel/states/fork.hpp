#pragma once

#include <await/cancel/detail/state.hpp>

#include <await/cancel/detail/state_variant.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace await::cancel {

namespace detail {

//////////////////////////////////////////////////////////////////////

// Progress guarantee: Lock-freedom

// Single-producer + multi-consumer

class ForkStateBase : public StateBase {
  static const uint32_t kInitial = 0;
  static const uint32_t kCancelRequested = 1;

  using State = StateVariant;

 public:
  ForkStateBase()
      : state_(State::Value(kInitial)) {
  }

  ~ForkStateBase();

  // IHandler

  // Release-reference operation
  void Forward(Signal) override;

  // StateBase

  bool Cancellable() const override {
    return true;
  }

  // Consumer

  // Lock-free
  bool AddHandler(HandlerBase* handler) override;

  // NB: Not implemented, use ReleaseHandlers
  void RemoveHandler(HandlerBase* handler) override;

  // Lock-free
  void ReleaseHandlers();

  // Wait-free
  bool HasHandlers() const override;

  // Wait-free
  bool CancelRequested() const override;

  // Single producer

  // Wait-free
  void RequestCancel() override;

 private:
  // Helpers

  static bool IsCancelRequested(State state) {
    return state.IsValue() && state.AsValue() == kCancelRequested;
  }

  static bool IsInitial(State state) {
    return state.IsValue() && state.AsValue() == kInitial;
  }

 private:
  twist::ed::stdlike::atomic<State> state_;
};

}  // namespace detail

}  // namespace await::cancel
