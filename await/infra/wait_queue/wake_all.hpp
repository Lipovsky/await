#pragma once

#include <twist/ed/stdlike/atomic.hpp>

namespace await::wait_queue {

// Lock-free intrusive wait queue for
// synchronization primitives with WakeAll-like API
// (e.g. WaitGroup or OneShotEvent)

//////////////////////////////////////////////////////////////////////

template <typename TWaiter>
concept SinglyLinkableWaiter = requires(TWaiter* waiter) {
  { waiter->next } -> std::same_as<TWaiter*&>;
};

//////////////////////////////////////////////////////////////////////

template <SinglyLinkableWaiter TWaiter>
class SealableAtomicQueue {
 public:
  ~SealableAtomicQueue() {
    assert(IsTerminalState(head_.load()));
  }

  bool TryAdd(TWaiter* waiter) {
    while (true) {
      TWaiter* head = head_.load();

      if (IsSealedState(head)) {
        return false;
      }

      waiter->next = head;
      if (head_.compare_exchange_weak(head, waiter)) {
        return true;
      }
    }
  }

  // One-shot
  TWaiter* Seal() {
    TWaiter* head = head_.exchange(SealedState());
    return Reverse(head);
  }

  bool Sealed() const {
    return IsSealedState(head_.load());
  }

 private:
  static TWaiter* SealedState() {
    return (TWaiter*)1;
  }

  static bool IsSealedState(TWaiter* state) {
    return (uintptr_t)state == 1;
  }

  static bool IsEmptyList(TWaiter* head) {
    return head == nullptr;
  }

  static bool IsTerminalState(TWaiter* state) {
    return IsSealedState(state) || IsEmptyList(state);
  }

  static TWaiter* Reverse(TWaiter* head) {
    // Empty list
    if (head == nullptr) {
      return nullptr;
    }

    TWaiter* prev = head;
    TWaiter* curr = head->next;

    head->next = nullptr;

    while (curr != nullptr) {
      TWaiter* next = curr->next;

      curr->next = prev;

      prev = curr;
      curr = next;
    }

    return prev;
  }

 private:
  // States:
  // 1) nullptr - Empty queue (initial state)
  // 2) TWaiter* - Non-empty queue
  // 3) 1 – Sealed queue
  twist::ed::stdlike::atomic<TWaiter*> head_{nullptr};
};

}  // namespace await::wait_queue
