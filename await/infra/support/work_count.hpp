#pragma once

#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/condition_variable.hpp>
#include <twist/ed/stdlike/atomic.hpp>

namespace await::support {

class WorkCount {
 public:
  void Add(size_t count = 1) {
    count_.fetch_add(count, std::memory_order::relaxed);
  }

  void Done(size_t count = 1) {
    if (count_.fetch_sub(count, std::memory_order::acq_rel) == count) {
      std::lock_guard guard(mutex_);
      idle_.notify_all();
    }
  }

  // Multi-shot
  void WaitIdle() {
    std::unique_lock lock(mutex_);
    while (count_.load() > 0) {
      idle_.wait(lock);
    }
  }

 private:
  twist::ed::stdlike::atomic<size_t> count_{0};
  twist::ed::stdlike::mutex mutex_;
  twist::ed::stdlike::condition_variable idle_;
};

}  // namespace await::support
