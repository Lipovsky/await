#include <await/thread/parking_lot.hpp>

#include <twist/ed/wait/futex.hpp>

namespace await::thread::system {

void ParkingLot::ParkIf(Epoch old) {
  twist::ed::futex::Wait(epoch_, old);
}

ParkingLot::Epoch ParkingLot::Prepare() {
  return epoch_.load();
}

void ParkingLot::Wake() {
  auto wake_key = twist::ed::futex::PrepareWake(epoch_);
  epoch_.fetch_add(1);
  twist::ed::futex::WakeOne(wake_key);
}

}  // namespace await::thread::system
