#pragma once

#include <carry/context.hpp>

namespace await::tasks::curr {

// Context of the current task
carry::Context Context();

}  // namespace await::tasks::curr
