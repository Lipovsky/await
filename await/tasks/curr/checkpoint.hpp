#pragma once

#include <await/tasks/core/cancelled.hpp>

namespace await::tasks::curr {

// Checks for cancellation request
void Checkpoint();

inline bool KeepRunning() {
  Checkpoint();
  return true;
}

}  // namespace await::tasks::curr
