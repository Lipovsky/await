#include <await/tasks/exe/pools/compute/thread_pool.hpp>

#include <twist/ed/local/ptr.hpp>

namespace await::tasks::pools::compute {

TWISTED_THREAD_LOCAL_PTR(ThreadPool, pool);

//////////////////////////////////////////////////////////////////////

ThreadPool::ThreadPool(size_t threads) {
  LaunchWorkers(/*count=*/threads);
}

ThreadPool::~ThreadPool() {
  DoStop();
}

// IExecutor

void ThreadPool::Submit(TaskBase* task, SchedulerHint) {
  task_count_.Add(1);
  if (!tasks_.Put(task)) {
    WHEELS_PANIC("Submit is not ordered with Stop");
  }
}

// Thread pool

void ThreadPool::WaitIdle() {
  task_count_.WaitIdle();
}

void ThreadPool::Stop() {
  DoStop();
}

ThreadPool* ThreadPool::Current() {
  return pool;
}

size_t ThreadPool::ThreadCount() const {
  return workers_.size();
}

void ThreadPool::LaunchWorkers(size_t count) {
  for (size_t i = 0; i < count; ++i) {
    workers_.emplace_back([this]() {
      WorkerRoutine();
    });
  }
}

// IScheduler
TaskBase* ThreadPool::PickTask() {
  return tasks_.Take();
}

// IScheduler
void ThreadPool::TaskCompleted() {
  task_count_.Done();
}

void ThreadPool::WorkerRoutine() {
  pool = this;

  while (TaskBase* next = PickTask()) {
    next->Run();
    TaskCompleted();
  }
}

void ThreadPool::JoinWorkers() {
  for (auto& worker : workers_) {
    worker.join();
  }
  workers_.clear();
}

void ThreadPool::DoStop() {
  tasks_.Close();
  JoinWorkers();
}

}  // namespace await::tasks::pools::compute
