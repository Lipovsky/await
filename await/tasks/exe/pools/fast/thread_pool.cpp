#include <await/tasks/exe/pools/fast/thread_pool.hpp>

#include <await/tasks/exe/pools/fast/runners/thread.hpp>

#include <await/tasks/exe/pools/fast/task_flags.hpp>

namespace await::tasks::pools::fast {

ThreadPool::ThreadPool(size_t threads)
    : ThreadPool(threads, Delayed{}) {
  Start();
}

ThreadPool::ThreadPool(size_t threads, Delayed)
    : threads_(threads),
      runner_(&runners::ThreadWorkerRunner::Instance()),
      coordinator_(threads) {
}

ThreadPool::~ThreadPool() {
  WHEELS_ASSERT(workers_.empty(), "Explicit Stop required");
}

void ThreadPool::SetRunner(IRunner& runner) {
  runner_ = &runner;
}

void ThreadPool::Start() {
  for (size_t i = 0; i < threads_; ++i) {
    workers_.emplace_back(*this, i);
  }

  for (auto& worker : workers_) {
    worker.Start();
  }
}

void ThreadPool::Submit(TaskBase* task, SchedulerHint hint) {
  Push(task, hint);
  coordinator_.NotifyNewTask();
}

IRunner& ThreadPool::Runner() {
  return *runner_;
}

bool ThreadPool::FromThisPool(Worker* submitter) const {
  return (submitter != nullptr) && (submitter->Host() == this);
}

void ThreadPool::PushExternal(TaskBase* task) {
  TaskFlags::Set(task->flags, TaskFlags::External);
  work_count_.Add(1);
  global_tasks_.PushOne(task);
}

void ThreadPool::Push(TaskBase* task, SchedulerHint hint) {
  //  if (hint == Hint::Yield) {
  //    global_tasks_.PushOne(task);
  //    return;
  //  }

  Worker* submitter = Worker::Current();

  if (FromThisPool(submitter)) {
    // Task submitted from worker thread
    submitter->LocalPush(task, hint);
  } else {
    // From external thread
    PushExternal(task);
  }
}

void ThreadPool::WaitIdle() {
  work_count_.WaitIdle();
}

void ThreadPool::Stop() {
  coordinator_.RequestStop();

  for (auto& worker : workers_) {
    worker.Join();
  }

  // Expect empty queue
  WHEELS_ASSERT(global_tasks_.TryPopOne() == nullptr, "Cannot Stop thread pool with pending tasks");

  for (auto& worker : workers_) {
    metrics_.Merge(worker.Metrics());
  }

  workers_.clear();
}

ThreadPool* ThreadPool::Current() {
  if (auto* this_worker = Worker::Current()) {
    return this_worker->Host();
  } else {
    return nullptr;
  }
}

size_t ThreadPool::ThreadCount() const {
  return workers_.size();
}

}  // namespace await::tasks::pools::fast
