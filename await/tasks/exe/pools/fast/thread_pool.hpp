#pragma once

#include <await/tasks/core/executor.hpp>

#include <await/tasks/exe/pools/fast/queues/global_queue.hpp>
#include <await/tasks/exe/pools/fast/coordinator.hpp>
#include <await/tasks/exe/pools/fast/worker.hpp>

#include <await/infra/support/work_count.hpp>

// random_device
#include <twist/ed/stdlike/random.hpp>
#include <twist/ed/stdlike/atomic.hpp>

#include <deque>

namespace await::tasks::pools::fast {

struct Delayed {};

// [Await.Executor]
class ThreadPool : public IExecutor {
  friend class Worker;

 public:
  explicit ThreadPool(size_t threads, Delayed);

  // Create and start
  explicit ThreadPool(size_t threads);

  ~ThreadPool();

  // Non-copyable
  ThreadPool(const ThreadPool&) = delete;
  ThreadPool& operator=(const ThreadPool&) = delete;

  // Non-movable
  ThreadPool(ThreadPool&&) = delete;
  ThreadPool& operator=(ThreadPool&&) = delete;

  // Setup (Delayed + Start)
  void SetRunner(IRunner& runner);

  void Start();

  // IExecutor
  void Submit(TaskBase* task, SchedulerHint hint) override;

  static ThreadPool* Current();

  size_t ThreadCount() const;

  void WaitIdle();

  // Hard shutdown: stop workers and discard pending tasks
  void Stop();

  // After Stop
  PoolMetrics GetMetrics() const {
    return metrics_;
  }

 private:
  IRunner& Runner();

  bool FromThisPool(Worker* submitter) const;
  void Push(TaskBase* task, SchedulerHint hint);
  void PushExternal(TaskBase* task);

 private:
  const size_t threads_;
  IRunner* runner_;

  Coordinator coordinator_;
  std::deque<Worker> workers_;
  GlobalQueue<TaskBase> global_tasks_;

  support::WorkCount work_count_;

  twist::ed::stdlike::random_device random_device_;

  PoolMetrics metrics_;
};

}  // namespace await::tasks::pools::fast
