#pragma once

#include <cstdlib>

namespace await::tasks::pools::fast {

struct WorkerMetrics {
  size_t tasks = 0;
  size_t tasks_lifo = 0;
  size_t tasks_local = 0;

  size_t steals = 0;
  size_t global_grabs = 0;

  size_t tasks_stolen = 0;
  size_t tasks_grabbed = 0;

  size_t parks = 0;
  size_t wakes = 0;
};

struct PoolMetrics : WorkerMetrics {
  void Merge(WorkerMetrics m) {
    steals += m.steals;
    global_grabs += m.global_grabs;

    tasks += m.tasks;

    tasks_lifo += m.tasks_lifo;
    tasks_local += m.tasks_local;

    tasks_stolen += m.tasks_stolen;
    tasks_grabbed += m.tasks_grabbed;

    wakes += m.wakes;
    parks += m.parks;
  }
};

}  // namespace await::tasks::pools::fast
