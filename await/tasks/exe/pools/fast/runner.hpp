#pragma once

#include <await/tasks/exe/pools/fast/scheduler.hpp>

namespace await::tasks {

// Runner runs tasks from the provided `scheduler`
// as a separate worker thread

struct IRunner {
  virtual ~IRunner() = default;

  // Run tasks until stop requested
  virtual void RunWorker(IScheduler& scheduler) = 0;
};

}  // namespace await::tasks
