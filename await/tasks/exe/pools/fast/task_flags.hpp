#pragma once

#include <cstdint>

namespace await::tasks::pools::fast {

struct TaskFlags {
  enum Flags : uintptr_t {
    External = 1,  // Submitted from external thread
  };

  // Operations

  static void Set(uintptr_t& mask, Flags f) {
    mask |= f;
  }

  static bool IsSet(uintptr_t mask, Flags f) {
    return (mask & f) != 0;
  }

  // Precondition: IsSet(mask, f)
  static void Reset(uintptr_t& mask, Flags f) {
    mask ^= f;
  }
};

}  // namespace await::tasks::pools::fast
