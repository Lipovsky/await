#pragma once

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/mutex.hpp>

#include <wheels/intrusive/list.hpp>

namespace await::tasks::pools::fast {

class Worker;

class Coordinator {
 public:
  explicit Coordinator(size_t threads)
      : threads_(threads) {
  }

  // NB: Invoked on every Submit, so do not write to
  // memory locations shared between worker threads on fast path!
  void NotifyNewTask() {
    if (ShouldWakeWorker()) {
      WakeWorker();
    }
  }

  void StepDownAsActiveWorker(Worker* worker);

  void BecomeActiveWorker(Worker* worker);

  void WakeWorker();

  bool StartSpinning();

  // Returns true for last spinning worker
  bool StopSpinning();

  // Stop

  void RequestStop() {
    stop_requested_.store(true);
    WakeAll();
  }

  bool StopRequested() const {
    return stop_requested_.load();
  }

 private:
  bool ShouldWakeWorker() const;

  void WakeAll();

  void IncrIdleCount();
  void DecrIdleCount();

 private:
  const size_t threads_;

  // (Idle, Spinning)
  twist::ed::stdlike::atomic<uint64_t> state_{0};

  twist::ed::stdlike::mutex mutex_;
  wheels::IntrusiveList<Worker> idle_;

  twist::ed::stdlike::atomic<bool> stop_requested_{false};
};

}  // namespace await::tasks::pools::fast
