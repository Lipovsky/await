#pragma once

#include <await/tasks/exe/pools/fast/runner.hpp>

namespace await::tasks::runners {

class ThreadWorkerRunner : public IRunner {
 public:
  void RunWorker(IScheduler& scheduler) override {
    while (TaskBase* next = scheduler.PickTask()) {
      next->Run();
    }
  }

  static ThreadWorkerRunner& Instance() {
    static ThreadWorkerRunner single;
    return single;
  }
};

}  // namespace await::tasks::runners
