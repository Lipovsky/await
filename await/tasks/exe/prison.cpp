#include <await/tasks/exe/prison.hpp>

#include <wheels/core/panic.hpp>

namespace await::tasks {

//////////////////////////////////////////////////////////////////////

// [Await.Executor]
class PrisonExecutor final : public IExecutor {
 public:
  void Submit(TaskBase*, SchedulerHint) override {
    WHEELS_PANIC("Task has been submitted to Prison executor");
  }
};

//////////////////////////////////////////////////////////////////////

IExecutor& Prison() {
  static PrisonExecutor instance;
  return instance;
}

}  // namespace await::tasks
