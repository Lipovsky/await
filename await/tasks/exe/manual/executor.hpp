#pragma once

#include <await/tasks/core/executor.hpp>

#include <cstdlib>

namespace await::tasks {

struct IManualExecutor : IExecutor {
  virtual ~IManualExecutor() = default;

  // Run tasks

  // Run tasks until queue becomes empty
  // Return number of completed tasks
  // Post-condition: HasTasks() == false
  virtual size_t Drain() = 0;

  // Run at most `limit` tasks from queue
  // Return number of completed tasks
  virtual size_t RunAtMost(size_t limit) = 0;

  bool RunNext() {
    return RunAtMost(1) == 1;
  }

  virtual bool HasTasks() const = 0;
  virtual size_t TaskCount() const = 0;
};

}  // namespace await::tasks
