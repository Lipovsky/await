#pragma once

#include <await/tasks/exe/manual/budget.hpp>

namespace await::tasks::manual::budget {

struct Unlimited final : IBudget {
  bool Allow() const override {
    return true;
  }

  void Withdraw() override {
    // No-op
  }
};

}  // namespace await::tasks::manual::budget
