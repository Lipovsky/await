#pragma once

#include <await/tasks/core/executor.hpp>

namespace await::tasks {

// Runs scheduled tasks immediately in the current thread
IExecutor& Inline();

};  // namespace await::tasks
