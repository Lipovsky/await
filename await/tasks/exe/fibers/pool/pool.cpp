#include <await/tasks/exe/fibers/pool/pool.hpp>

namespace await::tasks::fibers {

Pool::Pool(size_t threads, await::fibers::IResourceManager& manager)
    : runners::FiberTaskRunner(manager),
      tasks::pools::fast::ThreadPool(threads, {}) {
  SetRunner(*this);
  Start();
}

Pool::Pool(size_t threads)
    : Pool(threads, await::fibers::GlobalResourceManager()) {
}

}  // namespace await::tasks::fibers
