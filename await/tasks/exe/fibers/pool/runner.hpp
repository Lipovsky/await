#pragma once

#include <await/tasks/exe/pools/fast/runner.hpp>

#include <await/thread/awaiter.hpp>

#include <await/fibers/core/fwd.hpp>
#include <await/fibers/core/runnable.hpp>
#include <await/fibers/core/resource_manager.hpp>

namespace await::tasks::runners {

class FiberTaskRunner;

//////////////////////////////////////////////////////////////////////

class IdleCarrier : public thread::ISuspendingAwaiter,
                    public wheels::IntrusiveForwardListNode<IdleCarrier> {
 public:
  // By shadow worker
  void Resume(IScheduler& scheduler) {
    DoResume(&scheduler);
  }

  // By shadow worker
  void Stop() {
    DoResume(/*scheduler=*/nullptr);
  }

  // By resumed carrier fiber
  IScheduler* GetScheduler() {
    return scheduler_;
  }

  // ISuspendingAwaiter
  // By detached carrier fiber
  void AwaitSuspend(thread::Handle f) override;

 private:
  void DoResume(IScheduler* scheduler);

 private:
  fibers::Fiber* fiber_;
  IScheduler* scheduler_;
};

//////////////////////////////////////////////////////////////////////

class ShadowWorker : private fibers::IRunnable {
  friend class IdleCarrier;

 public:
  ShadowWorker(FiberTaskRunner& host, IScheduler& scheduler)
      : host_(host),
        scheduler_(scheduler) {
  }

  void Run();

 private:
  void Stop();

  void AddToPool(IdleCarrier* carrier);

  // IRunnable
  void RunCoro(void* bootstrap) noexcept override;

  static void RunCarrier(fibers::Fiber* self) noexcept;
  static IScheduler* AcquireScheduler(fibers::Fiber* self);

  IdleCarrier* TakeIdleCarrier();
  void StartNewCarrier();

 private:
  FiberTaskRunner& host_;
  IScheduler& scheduler_;

  using CarrierPool = wheels::IntrusiveForwardList<IdleCarrier>;

  CarrierPool carriers_;
};

//////////////////////////////////////////////////////////////////////

class FiberTaskRunner : public IRunner {
 public:
  // Use global fiber manager
  FiberTaskRunner();

  FiberTaskRunner(fibers::IResourceManager& manager)
      : manager_(manager) {
  }

  fibers::IResourceManager& ResourceManager() {
    return manager_;
  }

  void RunWorker(IScheduler& scheduler) override;

 private:
  fibers::IResourceManager& manager_;
};

}  // namespace await::tasks::runners
