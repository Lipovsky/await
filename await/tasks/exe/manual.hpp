#pragma once

#include <await/tasks/exe/manual/executor.hpp>
#include <await/tasks/exe/manual/budget.hpp>

#include <wheels/intrusive/forward_list.hpp>

namespace await::tasks {

// Single-threaded task queue for deterministic testing

// [Await.Executor]
class ManualExecutor final : public IManualExecutor {
 public:
  void Submit(TaskBase* task, SchedulerHint) override {
    tasks_.PushBack(task);
  }

  ~ManualExecutor();

  size_t RunAtMost(size_t limit) override;
  size_t Drain() override;

  bool HasTasks() const override {
    return !tasks_.IsEmpty();
  }

  size_t TaskCount() const override {
    return tasks_.Size();
  }

 private:
  size_t Run(manual::IBudget& budget);

 private:
  wheels::IntrusiveForwardList<TaskBase> tasks_;
};

}  // namespace await::tasks
