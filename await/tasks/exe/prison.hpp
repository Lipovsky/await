#include <await/tasks/core/executor.hpp>

namespace await::tasks {

// For tasks that should not run
IExecutor& Prison();

}  // namespace await::tasks
