#pragma once

#include <wheels/intrusive/forward_list.hpp>

namespace await::tasks {

//////////////////////////////////////////////////////////////////////

struct ITask {
  virtual ~ITask() = default;

  // NB: Release-reference operation!
  virtual void Run() noexcept = 0;
};

//////////////////////////////////////////////////////////////////////

// Intrusive task
struct TaskBase : ITask,
                  wheels::IntrusiveForwardListNode<TaskBase> {
  uintptr_t flags = 0;
};

}  // namespace await::tasks
