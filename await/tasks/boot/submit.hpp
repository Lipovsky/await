#pragma once

#include <await/futures/make/submit.hpp>
#include <await/futures/run/go.hpp>

namespace await::tasks {

//////////////////////////////////////////////////////////////////////

// For library bootstrapping
// Use futures::Submit instead

//////////////////////////////////////////////////////////////////////

/*
 * Usage:
 *
 * tasks::Submit(thread_pool, []() {
 *   fmt::println("Running in thread pool");
 * });
 *
 */

template <typename F>
void Submit(IExecutor& where, F fun) {
  futures::Submit(where, std::forward<F>(fun)) | futures::Go();
}

}  // namespace await::tasks
