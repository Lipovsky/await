#pragma once

#include <await/tasks/core/executor.hpp>
#include <await/fibers/core/runnable.hpp>

namespace await::fibers::boot {

void Go(IRunnable* runnable);

void Go(tasks::IExecutor& where, IRunnable* runnable);

}  // namespace await::fibers::boot
