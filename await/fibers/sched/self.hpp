#pragma once

#include <await/fibers/core/id.hpp>

#include <optional>
#include <string>

namespace await::fibers {

// Am I fiber or thread?
bool AmIFiber();

namespace self {

FiberId GetId();

std::optional<std::string> GetName();
void SetName(const std::string& name);

}  // namespace self

}  // namespace await::fibers
