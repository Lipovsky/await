#include <await/fibers/sched/self.hpp>

#include <await/fibers/core/fiber.hpp>

namespace await::fibers {

bool AmIFiber() {
  return Fiber::TrySelf() != nullptr;
}

namespace self {

FiberId GetId() {
  return Fiber::Self().GetId();
}

void SetName(const std::string& name) {
  Fiber::Self().SetName(name);
}

std::optional<std::string> GetName() {
  return Fiber::Self().GetName();
}

}  // namespace self

}  // namespace await::fibers
