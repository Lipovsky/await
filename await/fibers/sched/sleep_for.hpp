#pragma once

#include <await/timers/core/timer_keeper.hpp>

namespace await::fibers {

// Pause this fiber for a given delay

void SleepFor(timers::Delay delay);

}  // namespace await::fibers
