#pragma once

#include <await/tasks/core/executor.hpp>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

// Reschedule current fiber to executor `target`
void TeleportTo(tasks::IExecutor& target);

//////////////////////////////////////////////////////////////////////

/*
 * Example:
 *
 * {
 *    TeleportGuard guard(strand);
 *
 *    // Serialized with other strand tasks,
 *    // access shared data structures
 * }
 *
 */

class TeleportGuard {
 public:
  explicit TeleportGuard(tasks::IExecutor& to);

  ~TeleportGuard();

 private:
  tasks::IExecutor& home_;
};

}  // namespace await::fibers
