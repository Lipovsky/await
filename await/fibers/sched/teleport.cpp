#include <await/fibers/sched/teleport.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/fibers/core/fiber.hpp>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

void TeleportTo(tasks::IExecutor& target) {
  Fiber::Self().SetExecutor(&target);
  Yield();
}

//////////////////////////////////////////////////////////////////////

TeleportGuard::TeleportGuard(tasks::IExecutor& e)
    : home_(Fiber::Self().GetExecutor()) {
  TeleportTo(e);
}

TeleportGuard::~TeleportGuard() {
  TeleportTo(home_);
}

}  // namespace await::fibers
