#include <await/fibers/core/fiber.hpp>

#include <await/thread/this.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/core/defer.hpp>

#include <utility>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

Fiber& Fiber::Self() {
#if defined(NDEBUG)
  return *static_cast<Fiber*>(thread::This());
#else
  Fiber* self = TrySelf();
  WHEELS_ASSERT(self != nullptr, "Not in fiber context");
  return *self;
#endif
}

Fiber* Fiber::TrySelf() {
  return dynamic_cast<Fiber*>(thread::This());
}

//////////////////////////////////////////////////////////////////////

Fiber::Fiber(IRunnable* runnable,  //
             wheels::MutableMemView stack,    //
             IResourceManager& manager,          //
             tasks::IExecutor& executor,  //
             FiberId id)
    : stack_(stack),
      coroutine_( stack, runnable, /*bootstrap=*/this),
      resource_manager_(manager),
      id_(id) {
  SetExecutor(&executor);
}

Fiber::~Fiber() {
}

void Fiber::Suspend(thread::IAwaiter* awaiter) {
  awaiter_ = awaiter;
  Stop();
}

void Fiber::Resume(tasks::SchedulerHint hint) {
  Schedule(hint);
}

void Fiber::Schedule(tasks::SchedulerHint hint) {
  GetExecutor().Submit(this, hint);
}

void Fiber::ReleaseResources() {
  resource_manager_.ReleaseStack(stack_);
}

void Fiber::Step() {
  thread::Scope scope{this};
  coroutine_.Resume();
}

// Symmetric Transfer
void Fiber::RunChain(Fiber* start) {
  Fiber* next = start;

  do {
    next = next->DoRun();
  } while (next != nullptr);
}

void Fiber::Run() noexcept {
  RunChain(/*start=*/this);
}

Fiber* Fiber::DoRun() noexcept {
  Step();

  // Dispatch

  if (coroutine_.IsCompleted()) {
    DestroySelf();
    return nullptr;
  }

  thread::IAwaiter* awaiter = std::exchange(awaiter_, nullptr);
  WHEELS_ASSERT(awaiter != nullptr, "IsCompleted or awaiter expected");

  ++suspend_count_;
  auto next = awaiter->AwaitSymmetricSuspend({this});

  return next.IsValid() ? next.GetFiber() : nullptr;
}

void Fiber::DestroySelf() {
  ReleaseResources();
  delete this;
}

void Fiber::Stop() {
  coroutine_.Suspend();
}

std::optional<std::string> Fiber::GetName() const {
  return name_;
}

void Fiber::SetName(std::string name) {
  name_.emplace(name);
}

}  // namespace await::thread::fibers
