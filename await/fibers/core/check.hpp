#pragma once

#include <await/fibers/sched/self.hpp>

#include <wheels/core/assert.hpp>

#define AWAIT_I_AM_FIBER \
  WHEELS_ASSERT(::await::fibers::AmIFiber(), "Fiber expected, not thread")
