#pragma once

#include <wheels/memory/view.hpp>

namespace await::stacks {

struct IStackAllocator {
  virtual ~IStackAllocator() = default;

  virtual void SetStackSize(size_t bytes) = 0;

  virtual wheels::MutableMemView AllocateStack() = 0;
  virtual void ReleaseStack(wheels::MutableMemView) = 0;
};

}  // namespace await::stacks
