#pragma once

#include <cstdlib>

namespace await::fibers {

using FiberId = size_t;

static const FiberId kInvalidFiberId = 0;

}  // namespace await::fibers
