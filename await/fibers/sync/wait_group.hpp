#pragma once

#include <await/futures/impl/consumers/linked.hpp>

#include <await/futures/combine/seq/status.hpp>
#include <await/futures/combine/seq/start.hpp>

#include <await/cancel/manual.hpp>
#include <await/cancel/states/hub.hpp>
#include <await/cancel/token.hpp>

#include <await/infra/wait_queue/wake_all.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace await::fibers {

// https://gobyexample.com/waitgroups
// https://github.com/golang/go/blob/master/src/sync/waitgroup.go

// ~ Nursery
// https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/

// One-shot
class WaitGroup
    : private futures::IConsumer<bool> {

  // (count << 32) | waiter_count
  using State = uint64_t;

  using Unit = wheels::Unit;

  static const uint64_t kCountShift = 32;
  static const uint64_t kAddCount = (uint64_t)1 << kCountShift;

  static const uint64_t kAddWaiter = 1;

 private:
  enum class WaitEither {
    Ready,
    Queued,
  };

  struct Waiter : futures::IConsumer<Unit> {
    using ValueType = Unit;

    Waiter(WaitGroup& wg) : wg_(wg) {
      static_assert(futures::SomeFuture<Waiter>);
    }

    // Lazy protocol

    void Start(futures::IConsumer<Unit>* consumer) {
      consumer_ = futures::consumers::Link(consumer, wg_.cancel_state_.AsSource());

      if (wg_.AddWaiter(this) == WaitEither::Ready) {
        consumer_->Consume(wheels::Unit{});
      }
    }

    // futures::IConsumer<Unit>

    void Consume(futures::Output<wheels::Unit> output) noexcept override {
      consumer_->Consume(std::move(output));
    }

    void Cancel(futures::Context ctx) noexcept override {
      consumer_->Cancel(std::move(ctx));
    }

    cancel::Token CancelToken() override {
      std::abort();  // Never called
    }

    // SinglyLinkedWaiter

    Waiter* next = nullptr;

   private:
    WaitGroup& wg_;
    futures::consumers::LinkedConsumer<wheels::Unit> consumer_;
  };

 public:
  WaitGroup();
  ~WaitGroup();

  // += delta
  void Add(uint64_t delta);

  template <futures::SomeFuture Future>
  WaitGroup& Add(Future f) {
    Add(1);
    auto u = std::move(f) | futures::ToStatus() | futures::Start();
    u.Start(this);

    return *this;
  }

  // -= 1
  void Done();

  // One-shot, before Wait
  void RequestCancel();

  // One-shot
  void Wait();

 private:
  // futures::IConsumer<Unit>
  cancel::Token CancelToken() override;

  void Consume(futures::Output<bool>) noexcept override;
  void Cancel(futures::Context) noexcept override;

  // Fast path for Wait
  bool IsReady() const;

  WaitEither AddWaiter(Waiter* waiter);
  static void ResumeWaiters(Waiter* head);

  Waiter AsyncWait();

 private:
  static size_t GetWaiterCount(State state) {
    return state & (kAddCount - 1);
  }

  static size_t GetCount(State state) {
    return state >> kCountShift;
  }

 private:
  twist::ed::stdlike::atomic<State> state_{0};
  wait_queue::SealableAtomicQueue<Waiter> waiters_;
  cancel::ManualState<cancel::detail::HubStateBase> cancel_state_;
};

}  // namespace await::fibers
