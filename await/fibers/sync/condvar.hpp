#pragma once

#include <await/fibers/sync/mutex.hpp>
#include <await/fibers/sync/detail/futex.hpp>

namespace await::fibers {

// TODO: Remove?
class CondVar {
  using Lock = std::unique_lock<Mutex>;

 public:
  CondVar();

  void Wait(Lock& lock);

  void NotifyOne();
  void NotifyAll();

 private:
  detail::FutexLike<size_t> waiters_;
};

}  // namespace await::fibers
