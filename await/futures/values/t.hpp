#pragma once

// Customization points

#include <await/futures/impl/value/t/to_unit.hpp>
#include <await/futures/impl/value/t/timeout.hpp>

#include <await/futures/impl/value/t/map.hpp>
#include <await/futures/impl/value/t/and_then.hpp>
#include <await/futures/impl/value/t/or_else.hpp>

#include <await/futures/impl/value/t/result.hpp>

#include <await/futures/impl/value/t/interrupt.hpp>
#include <await/futures/impl/value/t/all.hpp>
#include <await/futures/impl/value/t/join.hpp>
