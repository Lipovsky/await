#pragma once

// Customization points

#include <await/futures/impl/value/expected/to_unit.hpp>
#include <await/futures/impl/value/expected/timeout.hpp>

#include <await/futures/impl/value/expected/is_monad.hpp>
#include <await/futures/impl/value/expected/map.hpp>
#include <await/futures/impl/value/expected/and_then.hpp>
#include <await/futures/impl/value/expected/or_else.hpp>

#include <await/futures/impl/value/expected/result.hpp>

#include <await/futures/impl/value/expected/interrupt.hpp>
#include <await/futures/impl/value/expected/all.hpp>
#include <await/futures/impl/value/expected/join.hpp>
