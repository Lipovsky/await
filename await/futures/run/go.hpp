#pragma once

#include <await/futures/impl/lazy/terminators/goer.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

template <SomeFuture Future>
void Go(Future f) {
  auto* goer = new lazy::terminators::Goer{std::move(f)};
  goer->Start();
}

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Goer {
  template <SomeFuture Future>
  void Pipe(Future input) {
    Go(std::move(input));
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

// Synonyms

inline auto Go() {
  return pipe::Goer();
}

inline auto Detach() {
  return pipe::Goer();
}

}  // namespace await::futures
