#pragma once

#include <await/await/algorithm.hpp>
#include <await/await/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct Await {
  template <SomeFuture Future>
  ValueOf<Future> Pipe(Future input) {
    return await::Await(std::move(input));
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Await() {
  return pipe::Await();
}

}  // namespace await::futures
