#pragma once

#include <await/futures/types/future.hpp>

#include <await/tasks/core/cancelled.hpp>

#include <coroutine>
#include <optional>

namespace await::futures {

template <SomeFuture Future>
class FutureAwaiter : IConsumer<typename Future::ValueType> {
  using ValueType = typename Future::ValueType;

 public:
  bool await_ready() {
    return false;
  }

  void await_suspend(std::coroutine_handle<> h) {
    coro_ = h;
    std::move(future_).Start(this);
  }

  ValueType await_resume() {
    if (value_) {
      return std::move(*value_);
    } else {
      throw tasks::CancelledException{};
    }
  }

 private:
  // IConsumer<ValueType>

  void Consume(Output<ValueType> o) noexcept override {
    value_.emplace(std::move(o.value));
    ResumeCoro();
  }

  void Cancel(Context) noexcept override {
    ResumeCoro();
  }

  void ResumeCoro() {
    coro_.resume();
  }

 private:
  Future future_;
  std::coroutine_handle<> coro_;
  std::optional<ValueType> value_;
};

}  // namespace await::futures

template <await::futures::SomeFuture Future>
auto operator co_await(Future f) {
  return await::futures::FutureAwaiter{std::move(f)};
}
