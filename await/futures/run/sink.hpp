#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/terminators/sink.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

template <SomeFuture Future>
using Chain = lazy::terminators::Sink<Future>;

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Sink {
  template <SomeFuture Future>
  Chain<Future> Pipe(Future input) {
    return lazy::terminators::Sink{std::move(input)};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Sink() {
  return pipe::Sink();
}

}  // namespace await::futures
