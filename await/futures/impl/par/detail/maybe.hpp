#pragma once

#include <optional>

namespace await::futures {

namespace combinators {

template <typename T>
struct Maybe {
  Maybe() = default;

  Maybe(T value)
      : data_(std::move(value)) {
  }

  explicit operator bool() const {
    return data_.has_value();
  }

  T& operator*() {
    return *data_;
  }

  std::optional<T> data_;
};

}  // namespace combinators

}  // namespace await::futures
