#pragma once

#include <twist/ed/stdlike/atomic.hpp>

namespace await::futures {

namespace detail {

//////////////////////////////////////////////////////////////////////

// TODO: memory orders

struct DoneOrCountStateMachine {
 private:
  // Representation: (count << 1) & (done bit)
  using State = uint64_t;

 public:
  // Returns counter value _after_ increment
  // or 0 if state machine in Done state
  size_t IncrementAndFetch() {
    State curr = state_.fetch_add(2);

    if (IsDone(curr)) {
      return 0;  // Ignore increment
    } else {
      return ToCount(curr) + 1;
    }
  }

  // Returns true if set for the first time
  bool Done() {
    return !IsDone(state_.fetch_or(1));
  }

  bool IsDone() const {
    return IsDone(state_.load());
  }

 private:
  static bool IsDone(State state) {
    return (state & 1) != 0;
  }

  static size_t ToCount(State state) {
    return state >> 1;
  }

 private:
  twist::ed::stdlike::atomic<State> state_{0};
};

}  // namespace detail

}  // namespace await::futures
