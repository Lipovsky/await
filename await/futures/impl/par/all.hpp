#pragma once

#include <await/futures/impl/value/t/all.hpp>

#include <await/futures/impl/par/detail/maybe.hpp>

#include <twist/ed/spin/lock.hpp>

#include <cassert>
#include <optional>
#include <vector>
#include <utility>

namespace await::futures {

namespace combinators {

template <typename InputType>
class AllCombinator {
 public:
  using Traits = value::AllTraits<InputType>;

  using OutputType = typename Traits::OutputType;

 public:
  AllCombinator(size_t inputs)
      : inputs_(inputs) {
    values_.reserve(inputs);
  }

  Maybe<OutputType> Combine(InputType input, CombinatorContext /*ctx*/) {
    std::unique_lock locker(mutex_);

    if (Traits::IsOk(input)) {
      values_.push_back(Traits::UnwrapValue(std::move(input)));

      if (values_.size() == inputs_) {
        completed_ = true;
        return Traits::WrapValues(std::move(values_));
      }
    } else {
      if (!std::exchange(completed_, true)) {
        return Traits::PropagateError(std::move(input));
      }
    }

    return {};  // Not ready
  }

  bool CombineCancel() {
    std::unique_lock locker(mutex_);

    return !std::exchange(completed_, true);
  }

  bool IsCompleted() const {
    return completed_;
  }

 private:
  const size_t inputs_;

  mutable twist::ed::SpinLock mutex_;
  // All values
  typename Traits::VectorType values_;
  bool completed_{false};
};

}  // namespace combinators

}  // namespace await::futures
