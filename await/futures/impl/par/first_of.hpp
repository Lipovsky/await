#pragma once

#include <await/futures/impl/par/detail/done_or_count.hpp>
#include <await/futures/impl/par/detail/maybe.hpp>

#include <await/futures/impl/value/t/result.hpp>

#include <optional>

namespace await::futures {

namespace combinators {

template <typename InputType>
class FirstOfCombinator {
 public:
  using Traits = value::ResultTraits<InputType>;

  using OutputType = InputType;

 public:
  FirstOfCombinator(size_t inputs)
      : inputs_(inputs) {
  }

  Maybe<OutputType> Combine(InputType input, CombinatorContext) {
    if (Traits::IsOk(input)) {
      // Success
      if (bool first = state_.Done()) {
        return std::move(input);  // First
      }
    } else {
      // Failure
      if (state_.IncrementAndFetch() == inputs_) {
        return std::move(input);  // Last
      }
    }
    return {};  // In progress
  }

  bool CombineCancel() {
    return state_.Done();
  }

  bool IsCompleted() const {
    return true;  // TODO
  }

 private:
  const size_t inputs_;

  detail::DoneOrCountStateMachine state_;
};

}  // namespace combinators

}  // namespace await::futures
