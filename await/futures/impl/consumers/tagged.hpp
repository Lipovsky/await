#pragma once

#include <await/futures/model/consumer.hpp>

namespace await::futures::consumers {

template <typename T, typename Tag>
struct ITaggedConsumer : IConsumer<T> {
  // IConsumer

  void Consume(Output<T> output) noexcept override {
    ConsumeTagged(std::move(output), Tag{});
  }

  void Cancel(Context ctx) noexcept override {
    CancelTagged(std::move(ctx), Tag{});
  }

  virtual void ConsumeTagged(Output<T>, Tag) noexcept = 0;
  virtual void CancelTagged(Context, Tag) noexcept = 0;
};

}  // namespace await::futures::consumers
