#pragma once

#include <await/futures/impl/value/t/all.hpp>

#include <optional>

namespace await::futures::value {

template <typename T>
struct AllTraits<std::optional<T>> {
  using VectorType = std::vector<T>;
  using OutputType = std::optional<VectorType>;

  static bool IsOk(const std::optional<T>& o) {
    return o.has_value();
  }

  static T UnwrapValue(std::optional<T> o) {
    return std::move(*o);
  }

  static OutputType WrapValues(VectorType vs) {
    return std::move(vs);
  }

  static OutputType PropagateError(std::optional<T> /*empty*/) {
    return std::nullopt;
  }
};

}  // namespace await::futures::value
