#pragma once

#include <await/futures/impl/value/t/or_else.hpp>

#include <await/futures/impl/value/optional/is.hpp>

#include <optional>
#include <type_traits>

namespace await::futures::value {

namespace apply {

template <typename T, typename F>
struct OrElse<std::optional<T>, F> {
  F f;

  // Mapper output type
  using U = std::invoke_result_t<F>;

  // Requirements for mapper output type
  static_assert(std::is_same_v<U, std::optional<T>>);

  auto operator()(std::optional<T> o) -> std::optional<T> {
    if (!o) {
      return f();
    } else {
      return std::move(o);
    }
  };
};

}  // namespace apply

}  // namespace await::futures::value
