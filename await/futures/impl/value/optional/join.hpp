#pragma once

#include <await/futures/impl/value/t/join.hpp>

#include <wheels/core/unit.hpp>

#include <optional>

namespace await::futures::value {

template <typename T>
struct JoinTraits<std::optional<T>> {
  using OutputType = std::optional<wheels::Unit>;

  static bool IsOk(const std::optional<T>& o) {
    return o.has_value();
  }

  static OutputType Ok() {
    return wheels::Unit{};
  }

  static OutputType PropagateError(std::optional<T> /*empty*/) {
    return std::nullopt;
  }
};

}  // namespace await::futures::value
