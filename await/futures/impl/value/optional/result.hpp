#pragma once

#include <await/futures/impl/value/t/result.hpp>

#include <optional>

namespace await::futures::value {

template <typename T>
struct ResultTraits<std::optional<T>> {
  static bool IsOk(const std::optional<T>& o) {
    return o.has_value();
  }
};

}  // namespace await::futures::value
