#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/value/t/map.hpp>
#include <await/futures/impl/value/optional/is.hpp>

#include <optional>

#include <type_traits>

namespace await::futures::value {

namespace apply {

// std::optional<T> -> (T -> U) -> std::optional<U>

template <typename T, typename F>
struct Map<std::optional<T>, F> {
  F f;

  // Mapper output type
  using U = std::invoke_result_t<F, T>;

  auto operator()(std::optional<T> o) -> std::optional<U> {
    if (o) {
      return f(std::move(*o));
    } else {
      return std::nullopt;
    }
  };
};

}  // namespace apply

}  // namespace await::futures::value
