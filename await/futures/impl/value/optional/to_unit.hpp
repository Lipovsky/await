#pragma once

#include <await/futures/impl/value/t/to_unit.hpp>

#include <wheels/core/unit.hpp>

#include <optional>

namespace await::futures::value {

template <typename T>
struct ToUnitTraits<std::optional<T>> {
  using UnitType = std::optional<wheels::Unit>;

  static UnitType ToUnit(std::optional<T> o) {
    if (o.has_value()) {
      return wheels::Unit{};
    } else {
      return std::nullopt;
    }
  }
};

}  // namespace await::futures::value
