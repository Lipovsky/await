#pragma once

#include <await/futures/impl/value/t/is_monad.hpp>

#include <optional>

namespace await::futures::value {

namespace match {

template <typename T>
struct IsMonad<std::optional<T>> : std::true_type {};

}  // namespace match

}  // namespace await::futures::value
