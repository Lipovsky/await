#pragma once

#include <await/futures/impl/value/t/and_then.hpp>
#include <await/futures/impl/value/expected/is.hpp>

#include <type_traits>

namespace await::futures::value {

namespace apply {

// Result<T> -> (T -> Result<U>) -> Result<U>

template <typename T, typename E, typename F>
struct AndThen<tl::expected<T, E>, F> {
  F f;

  // Mapper output type
  using R = std::invoke_result_t<F, T>;

  // Requirements for mapper output type
  static_assert(IsExpected<R>);

  auto operator()(tl::expected<T, E> e) -> R {
    return e.and_then(std::move(f));
  };
};

}  // namespace apply

}  // namespace await::futures::value
