#pragma once

#include <await/futures/impl/value/t/join.hpp>

#include <tl/expected.hpp>

namespace await::futures::value {

template <typename T, typename E>
struct JoinTraits<tl::expected<T, E>> {
  using OutputType = tl::expected<wheels::Unit, E>;

  static bool IsOk(const tl::expected<T, E>& e) {
    return e.has_value();
  }

  static OutputType Ok() {
    return wheels::Unit{};
  }

  static OutputType PropagateError(tl::expected<T, E> e) {
    return e.error();
  }
};

}  // namespace await::futures::value
