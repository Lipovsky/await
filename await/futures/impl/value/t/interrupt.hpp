#pragma once

#include <wheels/core/unit.hpp>

#include <optional>

namespace await::futures::value {

template <typename V, typename I>
struct InterruptTraits {
};

// T, T -> T

template <typename T>
concept NotUnit = !std::is_same_v<T, wheels::Unit>;

template <NotUnit T>
struct InterruptTraits<T, T> {
  using ValueType = T;

  static ValueType Value(T v) {
    return v;
  }

  static ValueType Interrupt(T i) {
    return i;
  }
};

// T, Unit -> std::optional<T>

template <typename T>
struct InterruptTraits<T, wheels::Unit> {
  using ValueType = std::optional<T>;

  static ValueType Value(T v) {
    return std::move(v);
  }

  static ValueType Interrupt(wheels::Unit) {
    return std::nullopt;
  }
};

}  // namespace await::futures::value
