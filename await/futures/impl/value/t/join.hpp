#pragma once

#include <wheels/core/unit.hpp>

namespace await::futures::value {

template <typename InputType>
struct JoinTraits {
  using OutputType = wheels::Unit;

  static bool IsOk(const InputType&) {
    return true;
  }

  static OutputType Ok() {
    return {};
  }

  static OutputType PropagateError(InputType) {
    std::abort();  // Never called
  }
};

}  // namespace await::futures::value
