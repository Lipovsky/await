#pragma once

#include <wheels/core/unit.hpp>

namespace await::futures::value {

template <typename T>
struct WithTimeoutTraits {
  static wheels::Unit Timeout() {
    return {};
  }
};

}  // namespace await::futures::value
