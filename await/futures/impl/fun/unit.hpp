#pragma once

#include <wheels/core/unit.hpp>

#include <type_traits>

namespace await::futures::fun {

template <typename F>
concept VoidInput = requires(F f) {
  f();
};

template <typename F>
concept NonVoidInput = !VoidInput<F>;

// UnitizeInput

template <typename F>
struct UnitizedInput {
  F f;

  auto operator()(wheels::Unit) {
    return f();
  }
};

template <VoidInput F>
auto UnitizeInput(F f) {
  return UnitizedInput<F>{std::move(f)};
}

template <NonVoidInput F>
auto UnitizeInput(F f) {
  return std::move(f);
}

template <typename F>
struct UnitizedOutput {
  F fun;

  UnitizedOutput(F f) : fun(std::move(f)) {
  }

  template <typename T>
  auto operator()(T input) {
    using U = std::invoke_result_t<F, T>;

    if constexpr (std::is_void_v<U>) {
      fun(std::move(input));
      return wheels::Unit{};
    } else {
      return fun(std::move(input));
    }
  }
};

template <typename F>
auto Unitize(F f) {
  return UnitizedOutput{
      UnitizeInput(std::move(f))};
}

}  // namespace await::futures::fun
