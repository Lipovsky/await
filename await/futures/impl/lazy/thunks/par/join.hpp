#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/consumers/linked.hpp>

#include <await/futures/impl/lazy/thunks/seq/box.hpp>

#include <await/cancel/states/fork.hpp>

#include <await/tasks/exe/inline.hpp>
#include <carry/empty.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::lazy {

namespace thunks {

// Join combinator with homogenous inputs

template <typename Inputs, template <typename> class Combinator>
class [[nodiscard]] Join
    : public IBoxedThunk<typename Combinator<typename Inputs::ValueType>::OutputType>,
      private IConsumer<typename Inputs::ValueType>,
      public cancel::detail::ForkStateBase,
      public refer::RefCounted<thunks::Join<Inputs, Combinator>> {
 public:
  using Self = thunks::Join<Inputs, Combinator>;

  using InputType = typename Inputs::ValueType;
  using OutputType = typename Combinator<InputType>::OutputType;

  using ValueType = OutputType;

 public:
  // Arguments for combinator
  template <typename... Args>
  Join(Inputs inputs, Args&&... args)
      : inputs_(std::move(inputs)),
        combinator_(inputs_.Count(), std::forward<Args>(args)...) {
  }

  ~Join() {
    assert(combinator_.IsCompleted());
  }

  // Lazy protocol

  void Start(IConsumer<OutputType>* consumer) override {
    consumer_ = consumers::Link(consumer, CancelSource());
    inputs_.Start(this);
  }

  // Inputs

  // Used by Inputs::Start
  IConsumer<InputType>* AsConsumer() {
    AddRef();
    return this;
  }

 private:
  // IConsumer<InputType>

  void Consume(Output<InputType> input) noexcept override {
    Combine(std::move(input.value), std::move(input.context.combinator));
    ReleaseRef();
  }

  void Cancel(Context) noexcept override {
    CombineCancel();
    ReleaseRef();
  }

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  void Combine(InputType input, CombinatorContext ctx) noexcept {
    if (auto output = combinator_.Combine(std::move(input), std::move(ctx))) {
      Complete(std::move(*output));
    }
  }

  void CombineCancel() {
    // Cancellation from upstream
    assert(Self::CancelRequested());

    if (combinator_.CombineCancel()) {
      consumer_->Cancel(Context{});
    }
  }

  void Complete(OutputType output) {
    // Send cancellation request downstream
    Self::RequestCancel();

    // Propagate output upstream
    consumer_->Consume(std::move(output));
  }

 private:
  Inputs inputs_;
  Combinator<InputType> combinator_;
  consumers::LinkedConsumer<OutputType> consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
