#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/lazy/thunks/seq/box.hpp>

#include <await/futures/impl/consumers/linked.hpp>
#include <await/futures/impl/consumers/tagged.hpp>

#include <await/cancel/states/fork.hpp>

#include <refer/ref_counted.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

class InterruptStateMachine {
  struct States {
    enum _ {
      Initial = 0,
      Completed = 1,
      Interrupted = 2,
      Cancelled = 4
    };
  };

 public:
  bool Complete() {
    return state_.exchange(States::Completed) == States::Initial;
  }

  bool Interrupt() {
    return state_.exchange(States::Interrupted) == States::Initial;
  }

  bool Cancel() {
    return state_.exchange(States::Cancelled) == States::Initial;
  }

 private:
  twist::ed::stdlike::atomic<uint64_t> state_{States::Initial};
};

//////////////////////////////////////////////////////////////////////

struct WithInterruptTags {
  struct Value {};
  struct Interrupt {};
};

template <typename Future, typename InterruptFuture>
struct WithInterruptTraits {
  using ValueType = typename value::InterruptTraits<typename Future::ValueType, typename InterruptFuture::ValueType>::ValueType;
};

//////////////////////////////////////////////////////////////////////

// [Await.FutureState]
template <typename Future, typename InterruptFuture>
class [[nodiscard]] WithInterrupt final
    : public IBoxedThunk<typename WithInterruptTraits<Future, InterruptFuture>::ValueType>,
      private futures::consumers::ITaggedConsumer<typename Future::ValueType, WithInterruptTags::Value>,
      private futures::consumers::ITaggedConsumer<typename InterruptFuture::ValueType, WithInterruptTags::Interrupt>,
      public cancel::detail::ForkStateBase,
      public refer::RefCounted<WithInterrupt<Future, InterruptFuture>> {
  using Self = WithInterrupt<Future, InterruptFuture>;

 public:
  using InputValueType = typename Future::ValueType;
  using InterruptType = typename InterruptFuture::ValueType;

  using InterruptTraits = value::InterruptTraits<InputValueType, InterruptType>;

  using ValueType = typename InterruptTraits::ValueType;

 public:
  WithInterrupt(Future future, InterruptFuture interrupt)
      : future_(std::move(future)),
        interrupt_(std::move(interrupt)) {
  }

  ~WithInterrupt() {
    Self::ReleaseHandlers();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) override {
    consumer_ = consumers::Link(consumer, CancelSource());

    future_.Start(AsValueConsumer());
    interrupt_.Start(AsInterruptConsumer());
  }

 private:
  // Value consumer

  IConsumer<InputValueType>* AsValueConsumer() {
    Self::AddRef();
    return static_cast<futures::consumers::ITaggedConsumer<InputValueType, WithInterruptTags::Value>*>(this);
  }

  void ConsumeTagged(Output<InputValueType> output,
                     WithInterruptTags::Value) noexcept override {
    CombineComplete(std::move(output));
    Self::ReleaseRef();
  }

  void CancelTagged(Context ctx, WithInterruptTags::Value) noexcept override {
    CombineCancel(std::move(ctx));
    Self::ReleaseRef();
  }

  // Interrupt consumer

  IConsumer<InterruptType>* AsInterruptConsumer() {
    Self::AddRef();
    return static_cast<
        futures::consumers::ITaggedConsumer<InterruptType, WithInterruptTags::Interrupt>*>(this);
  }

  void ConsumeTagged(Output<InterruptType> output,
                     WithInterruptTags::Interrupt) noexcept override {
    CombineInterrupt(std::move(output));
    Self::ReleaseRef();
  }

  void CancelTagged(Context ctx, WithInterruptTags::Interrupt) noexcept override {
    CombineCancel(std::move(ctx));
    Self::ReleaseRef();
  }

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  void CombineComplete(Output<InputValueType> output) {
    if (bool first = state_.Complete()) {
      // Propagate output upstream
      consumer_->Consume(
          {InterruptTraits::Value(std::move(output.value)), std::move(output.context)});
    }
  }

  void CombineInterrupt(Output<InterruptType> interrupt) {
    if (bool first = state_.Interrupt()) {
      // Structured concurrency time!

      // Propagate cancel downstream
      Self::RequestCancel();

      consumer_->Consume(
          {InterruptTraits::Interrupt(std::move(interrupt.value)), std::move(interrupt.context)});
    }
  }

  void CombineCancel(Context) {
    // Cancel from upstream
    assert(Self::CancelRequested());

    if (bool first = state_.Cancel()) {
      consumer_->Cancel(Context{});
    }
  }

 private:
  Future future_;
  InterruptFuture interrupt_;

  InterruptStateMachine state_;

  consumers::LinkedConsumer<ValueType> consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
