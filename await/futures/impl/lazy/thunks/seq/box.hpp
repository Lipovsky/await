#pragma once

#include <await/futures/model/thunk.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

// Thunk type erasure

//////////////////////////////////////////////////////////////////////

template <typename V>
struct IBoxedThunk : virtual refer::IManaged {
  virtual ~IBoxedThunk() = default;

  // Lazy protocol

  virtual void Start(IConsumer<V>* consumer) = 0;
};

//////////////////////////////////////////////////////////////////////

template <typename V>
using ThunkRef = refer::Ref<IBoxedThunk<V>>;

//////////////////////////////////////////////////////////////////////

namespace detail {

template <Thunk Boxed>
class Box : public IBoxedThunk<typename Boxed::ValueType>,
            public refer::RefCounted<Box<Boxed>> {
 public:
  using ValueType = typename Boxed::ValueType;

 public:
  Box(Boxed&& thunk)
      : thunk_(std::move(thunk)) {
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) override {
    thunk_.Start(consumer);
  }

 private:
  Boxed thunk_;
};

}  // namespace detail

//////////////////////////////////////////////////////////////////////

template <Thunk T>
ThunkRef<typename T::ValueType> Wrap(T&& thunk) {
  return refer::New<detail::Box<T>>(std::move(thunk));
}

//////////////////////////////////////////////////////////////////////

// Thunk

template <typename V>
class [[nodiscard]] Box final : private IConsumer<V> {
 public:
  using ValueType = V;

 public:
  Box(ThunkRef<V> box)
      : box_(std::move(box)) {
  }

  // Auto-Boxing

  template <Thunk T>
  Box(T thunk)
      : Box(Wrap(std::move(thunk))) {
  }

  // Non-copyable
  Box(const Box&) = delete;
  Box& operator=(const Box&) = delete;

  // Movable
  Box(Box&&) = default;
  Box& operator=(Box&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;

    auto box = box_;
    box->Start(this);
  }

 private:
  // IConsumer<ValueType>

  void Consume(Output<ValueType> output) noexcept override {
    box_.Reset();
    consumer_->Consume(std::move(output));
  }

  void Cancel(Context ctx) noexcept override {
    box_.Reset();
    consumer_->Cancel(std::move(ctx));
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  ThunkRef<V> box_;

  IConsumer<ValueType>* consumer_ = nullptr;
};

}  // namespace thunks

}  // namespace await::futures::lazy
