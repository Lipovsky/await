#pragma once

#include <await/futures/model/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] Via final
    : private IConsumer<typename Wrapped::ValueType> {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Via(Wrapped&& thunk, tasks::IExecutor* executor)
      : wrapped_(std::move(thunk)),
        executor_(executor) {
  }

  // Non-copyable
  Via(const Via&) = delete;

  // Movable
  Via(Via&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    wrapped_.Start(this);
  }

 private:
  // IConsumer

  void Consume(Output<ValueType> output) noexcept override {
    output.context.exe.executor = executor_;
    consumer_->Consume(std::move(output));
  }

  void Cancel(Context ctx) noexcept override {
    ctx.exe.executor = executor_;
    consumer_->Cancel(std::move(ctx));
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Wrapped wrapped_;
  await::tasks::IExecutor* executor_;

  IConsumer<ValueType>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy
