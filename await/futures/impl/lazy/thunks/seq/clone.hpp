#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/race/rendezvous.hpp>

#include <await/cancel/states/join.hpp>
#include <await/cancel/states/strand.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

template <typename T>
struct IForker : virtual refer::IManaged {
  virtual ~IForker() = default;

  virtual void Start(size_t index, IConsumer<T>* consumer) = 0;
  virtual void RequestCancel() = 0;
};

//////////////////////////////////////////////////////////////////////

template <Thunk F>
class Forker : public IForker<typename F::ValueType>,
               private IConsumer<typename F::ValueType>,
               public cancel::detail::JoinStateBase,
               public refer::RefCounted<Forker<F>> {

  using ValueType = typename F::ValueType;

  using Self = Forker<F>;

 public:
  Forker(F input)
      : cancel::detail::JoinStateBase(/*inputs=*/2),
        input_(std::move(input)) {
  }

  ~Forker() {
    //
  }

  // IForker

  void Start(size_t index, IConsumer<ValueType>* consumer) override {
    if (bool rendezvous = outputs_[index].SetConsumer(consumers::Link(consumer, CancelSource())); !rendezvous) {
      TryStart();
    }
  }

  void RequestCancel() override {
    cancel::detail::JoinStateBase::RequestCancel();
  }

 private:
  // IConsumer<T>

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

  void Consume(Output<ValueType> output) noexcept override {
    outputs_[0].SetOutput(output);
    outputs_[1].SetOutput(output);

    Complete();
  }

  void Cancel(Context ctx) noexcept override {
    outputs_[0].SetCancel(ctx);
    outputs_[1].SetCancel(ctx);

    Complete();
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  IConsumer<ValueType>* AsConsumer() {
    this->AddRef();
    return this;
  }

  void Complete() {
    this->ReleaseRef();
  }

  void TryStart() {
    if (!started_.exchange(true)) {
      input_.Start(AsConsumer());
    }
  }

 private:
  F input_;
  race::Rendezvous<ValueType> outputs_[2];
  twist::ed::stdlike::atomic<bool> started_{false};
};

//////////////////////////////////////////////////////////////////////

template <typename T>
using ForkerRef = refer::Ref<IForker<T>>;


template <Thunk Input>
ForkerRef<typename Input::ValueType> Fork(Input input) {
  return refer::New<Forker<Input>>(std::move(input));
}

//////////////////////////////////////////////////////////////////////

template <typename T>
class [[nodiscard]] Dolly {
  using ForkerRef = refer::Ref<IForker<T>>;

 public:
  using ValueType = T;

 public:
  Dolly(ForkerRef forker, size_t index)
      : forker_(forker),
        index_(index) {
    // Ok
  }

  // Non-copyable
  Dolly(const Dolly&) = delete;
  Dolly& operator=(const Dolly&) = delete;

  // Movable
  Dolly(Dolly&&) = default;

  ~Dolly() {
    if (forker_) {
      Release()->RequestCancel();
    }
  }

  void Start(IConsumer<T>* consumer) {
    Release()->Start(index_, consumer);
  }

  void RequestCancel() {
    Release()->RequestCancel();
  }

 private:
  ForkerRef Release() {
    return std::move(forker_);
  }

 private:
  ForkerRef forker_;
  size_t index_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
