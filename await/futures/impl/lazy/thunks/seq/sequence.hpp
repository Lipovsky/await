#pragma once

#include <await/futures/model/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk First, Thunk Then>
class [[nodiscard]] Sequence final : public IConsumer<typename First::ValueType> {
 public:
  using ValueType = typename Then::ValueType;

 public:
  Sequence(First first, Then then)
      : first_(std::move(first)),
        then_(std::move(then)) {
    static_assert(Thunk<Sequence>);
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    first_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<First::ValueType>

  void Consume(Output<typename First::ValueType> /*ignored*/) noexcept override {
    if (CancelRequested()) {
      // TODO: Context?
      consumer_->Cancel(Context{});
    } else {
      // Continue
      then_.Start(consumer_);
    }
  }

  void Cancel(Context ctx) noexcept override {
    // Skip `Then`
    consumer_->Cancel(std::move(ctx));
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  bool CancelRequested() const {
    return consumer_->CancelToken().CancelRequested();
  }

 private:
  First first_;
  Then then_;

  IConsumer<ValueType>* consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
