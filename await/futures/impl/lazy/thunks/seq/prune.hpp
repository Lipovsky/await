#pragma once

#include <await/futures/model/thunk.hpp>

#include <utility>

namespace await::futures::lazy {

namespace thunks {

template <typename V>
struct ISegment {
  virtual ~ISegment() = default;

  virtual void Start(IConsumer<V>* consumer) = 0;
};

template <Thunk T>
class Segment :
    public ISegment<typename T::ValueType>,
    private IConsumer<typename T::ValueType> {

  using V = typename T::ValueType;

 public:
  explicit Segment(T thunk)
      : thunk_(std::move(thunk)) {
  }

  void Start(IConsumer<V>* consumer) noexcept override {
    consumer_ = consumer;
    thunk_.Start(this);
  }

  // IConsumer<V>

  void Consume(Output<V> o) noexcept override {
    consumer_->Consume(std::move(o));
    Prune();
  }

  void Cancel(Context ctx) noexcept override {
    consumer_->Cancel(std::move(ctx));
    Prune();
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  void Prune() {
    delete this;
  }

 private:
  T thunk_;
  IConsumer<V>* consumer_;
};

template <typename V>
class [[nodiscard]] Pruned {
 public:
  using ValueType = V;

  template <Thunk T>
  explicit Pruned(T thunk) {
    segment_ = new Segment{std::move(thunk)};
  }

  // Non-copyable
  Pruned(const Pruned&) = delete;

  // Movable
  Pruned(Pruned&& that)
      : segment_(std::exchange(that.segment_, nullptr)) {
  }

  void Start(IConsumer<V>* consumer) {
    Release()->Start(consumer);
  }

 private:
  ISegment<V>* Release() {
    return std::exchange(segment_, nullptr);
  }

 private:
  ISegment<V>* segment_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
