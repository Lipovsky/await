#pragma once

#include <await/futures/model/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] Id {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Id(Wrapped&& thunk)
      : wrapped_(std::move(thunk)) {
  }

  // Non-copyable
  Id(const Id&) = delete;

  // Movable
  Id(Id&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    wrapped_.Start(consumer);
  }

 private:
  Wrapped wrapped_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
