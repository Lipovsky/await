#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/timers/core/timer_keeper.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

class [[nodiscard]] After final : private timers::TimerBase {
 public:
  using ValueType = wheels::Unit;

 public:
  After(timers::Delay delay)
      : delay_(delay) {
  }

  // Non-copyable
  After(const After&) = delete;

  // Movable
  After(After&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    delay_.timer_keeper.AddTimer(this);
  }

 private:
  // timers::ITimer

  std::chrono::milliseconds Delay() const override {
    return delay_.millis;
  }

  bool Periodic() const override {
    return false;
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  // timers::ITimerHandler

  void Alarm() noexcept override {
    consumer_->Consume(wheels::Unit{});
  }

 private:
  timers::Delay delay_;

  IConsumer<ValueType>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy
