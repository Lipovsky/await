#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/cancel/detail/handler.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

class [[nodiscard]] Never final : private cancel::HandlerBase {
 public:
  using ValueType = wheels::Unit;

 public:
  Never() = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    consumer_->CancelToken().Subscribe(this);
  }

 private:
  // cancel::IHandler

  void Forward(cancel::Signal signal) override {
    if (signal.CancelRequested()) {
      consumer_->Cancel(Context{});
    }
  }

 private:
  IConsumer<ValueType>* consumer_ = nullptr;
};

}  // namespace thunks

}  // namespace await::futures::lazy
