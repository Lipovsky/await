#pragma once

#include <await/futures/types/future.hpp>
#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/lazy/thunks/seq/eager.hpp>
#include <await/futures/impl/lazy/thunks/seq/input.hpp>

#include <vector>

namespace await::futures::lazy {

namespace detail {

//////////////////////////////////////////////////////////////////////

template <SomeFuture Future>
class VectorInputs {
 public:
  using ValueType = ValueOf<Future>;

 public:
  VectorInputs(std::vector<Future> fs)
      : futures_(std::move(fs)) {
  }

  size_t Count() const {
    return futures_.size();
  }

  template <typename Combinator>
  void Start(Combinator* combinator) {
    for (auto& f : futures_) {
      std::move(f).Start(combinator->AsConsumer());
    }
  }

 private:
  std::vector<Future> futures_;
};

//////////////////////////////////////////////////////////////////////

template <SomeFuture Future>
struct VectorInputsMaker {
  static auto ToInputs(std::vector<Future> futures) {
    using InputFuture = decltype(thunks::Input(std::move(futures.front())));

    std::vector<InputFuture> inputs;
    inputs.reserve(futures.size());

    // Add cancel state
    size_t index = 0;
    for (auto& f : futures) {
      inputs.push_back(thunks::Input{std::move(f), index++});
    }

    return VectorInputs(std::move(inputs));
  }
};

template <typename T>
struct VectorInputsMaker<thunks::Eager<T>> {
  static auto ToInputs(std::vector<thunks::Eager<T>> futures) {
    return VectorInputs(std::move(futures));
  }
};

template <SomeFuture Future>
auto ToVectorInputs(std::vector<Future> futures) {
  return VectorInputsMaker<Future>::ToInputs(std::move(futures));
}

}  // namespace detail

}  // namespace await::futures::lazy
