#pragma once

#include <await/futures/combine/par/first_of.hpp>

/*
 * "Or" operator (&&, or)
 * (f or g) means FirstOf(f, g)
 *
 */

template <await::futures::SomeFuture LeftFuture, await::futures::SomeFuture RightFuture>
await::futures::SomeFuture auto operator ||(LeftFuture lhs, RightFuture rhs) {
  return await::futures::FirstOf(std::move(lhs), std::move(rhs));
}
