#pragma once

#include <await/futures/impl/lazy/thunks/seq/eager.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures {

template <typename T>
using EagerFuture = lazy::thunks::Eager<T>;

using EagerUnitFuture = EagerFuture<wheels::Unit>;

}  // namespace await::futures
