#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/bomb.hpp>

namespace await::futures {

// Explodes (std::terminate) on start
// For testing purposes

inline UnitFuture auto Bomb() {
  return lazy::thunks::Bomb{};
}

}  // namespace await::futures
