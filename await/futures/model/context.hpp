#pragma once

#include <await/tasks/core/executor.hpp>

// Defaults
#include <await/tasks/exe/inline.hpp>

// User context
#include <carry/context.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

struct ExecutionContext {
  tasks::IExecutor* executor;
  tasks::SchedulerHint hint;

  ExecutionContext(tasks::IExecutor& e, tasks::SchedulerHint h)
      : executor(&e), hint(h) {
  }

  ExecutionContext()
      : ExecutionContext(
            tasks::Inline(),
            tasks::SchedulerHint::New) {
  }
};

//////////////////////////////////////////////////////////////////////

struct CombinatorContext {
  size_t index = 0;
};

//////////////////////////////////////////////////////////////////////

struct Context {
  ExecutionContext exe;
  CombinatorContext combinator;
  carry::Context user;
};

}  // namespace await::futures
