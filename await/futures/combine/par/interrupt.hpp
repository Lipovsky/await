#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/lazy/thunks/par/interrupt.hpp>

#include <await/futures/combine/seq/box.hpp>
#include <await/futures/combine/seq/input.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace detail {

template <SomeFuture MainFuture, SomeFuture InterruptFuture>
SomeFuture auto Interrupt(MainFuture main, InterruptFuture interrupt) {
  using T = typename value::InterruptTraits<ValueOf<MainFuture>, ValueOf<InterruptFuture>>::ValueType;

  auto thunk = refer::New<lazy::thunks::WithInterrupt<MainFuture, InterruptFuture>>(
      std::move(main), std::move(interrupt));

  return lazy::thunks::Box<T>{std::move(thunk)};
}

}  // namespace detail

//////////////////////////////////////////////////////////////////////

/*
 * Interrupt combinator
 *
 * Adds interruption condition for the given future
 *
 * Example:
 *
 * auto call = Rpc()
 *
 * auto call_with_timeout = Interrupt(
 *    std::move(call),
 *    futures::After(timeout));
 *
 */

template <SomeFuture MainFuture, SomeFuture InterruptFuture>
SomeFuture auto Interrupt(MainFuture future, InterruptFuture interrupt) {
  return detail::Interrupt(
      std::move(future) | ToInput(0),
      std::move(interrupt) | ToInput(1));
}

}  // namespace await::futures
