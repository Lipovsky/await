#pragma once

#include <await/futures/impl/lazy/thunks/seq/box.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Box {
  template <SomeFuture InputFuture>
  Future<ValueOf<InputFuture>> auto Pipe(InputFuture input) {
    return lazy::thunks::Box{
        lazy::thunks::Wrap(std::move(input))};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Box() {
  return pipe::Box{};
}

//////////////////////////////////////////////////////////////////////

// Alias

template <typename V>
using BoxedFuture = lazy::thunks::Box<V>;

}  // namespace await::futures
