#pragma once

#include <await/futures/impl/lazy/thunks/seq/map.hpp>
#include <await/futures/impl/fun/unit.hpp>
#include <await/futures/impl/value/t/map.hpp>
#include <await/futures/impl/value/t/is_monad.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

template <typename F>
struct [[nodiscard]] Map {
  F fun;

  Map(F f)
      : fun(std::move(f)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    static_assert(value::IsMonad<ValueOf<Future>>);

    return lazy::thunks::Map{std::move(input), futures::value::apply::Map<ValueOf<Future>, F>{std::move(fun)}};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

template <typename F>
auto Map(F fun) {
  return pipe::Map{fun::Unitize(std::move(fun))};
}

}  // namespace await::futures
