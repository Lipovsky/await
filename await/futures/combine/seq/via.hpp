#pragma once

#include <await/futures/impl/lazy/thunks/seq/via.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Via {
  tasks::IExecutor* executor;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Via{std::move(input), executor};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Via(tasks::IExecutor& executor) {
  return pipe::Via{&executor};
}

}  // namespace await::futures
