#pragma once

#include <await/futures/impl/lazy/thunks/seq/sched.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Sched {
  tasks::SchedulerHint hint;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Sched{std::move(input), hint};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Sched(tasks::SchedulerHint hint) {
  return pipe::Sched{hint};
}

inline auto Yield() {
  return Sched(tasks::SchedulerHint::Yield);
}

}  // namespace await::futures
