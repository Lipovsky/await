#pragma once

#include <await/futures/impl/lazy/thunks/seq/map.hpp>
#include <await/futures/impl/fun/unit.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

template <typename F>
struct [[nodiscard]] Apply {
  F fun;

  Apply(F f)
      : fun(std::move(f)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Map{std::move(input), std::move(fun)};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

template <typename F>
auto Apply(F fun) {
  return pipe::Apply{fun::Unitize(std::move(fun))};
}

}  // namespace await::futures
