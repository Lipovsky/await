#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/seq/clone.hpp>

#include <await/futures/syntax/pipe.hpp>

#include <utility>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {
struct [[nodiscard]] Clone {
  template <SomeFuture F>
  auto Pipe(F input) {
    using ValueType = typename F::ValueType;

    static_assert(std::is_copy_constructible_v<ValueType>);

    auto fork = lazy::thunks::Fork(std::move(input));

    return std::make_pair(
        lazy::thunks::Dolly<ValueType>(fork, 0),
        lazy::thunks::Dolly<ValueType>(fork, 1));
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Clone() {
  return pipe::Clone{};
}

}  // namespace await::futures
