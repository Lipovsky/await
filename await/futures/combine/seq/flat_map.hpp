#pragma once

#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/flatten.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

template <typename Mapper>
struct [[nodiscard]] FlatMap {
  Mapper mapper;

  FlatMap(Mapper m)
      : mapper(std::move(m)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return std::move(input) |
           futures::Map(std::move(mapper)) |
           futures::Flatten();
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

// Shortcut for: f | Map(mapper) | Flatten()

template <typename Mapper>
auto FlatMap(Mapper mapper) {
  return pipe::FlatMap{std::move(mapper)};
}

}  // namespace await::futures
