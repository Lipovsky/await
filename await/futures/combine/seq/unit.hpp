#pragma once

#include <await/futures/impl/lazy/thunks/seq/unit.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

template <SomeFuture Future>
SomeFuture auto ToUnit(Future future) {
  return lazy::thunks::ToUnit{std::move(future)};
}

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] ToUnit {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    return futures::ToUnit(std::move(input));
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto ToUnit() {
  return pipe::ToUnit();
}

}  // namespace await::futures
