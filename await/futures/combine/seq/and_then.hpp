#pragma once

#include <await/futures/impl/lazy/thunks/seq/map.hpp>
#include <await/futures/impl/fun/unit.hpp>
#include <await/futures/impl/value/t/and_then.hpp>
#include <await/futures/impl/value/t/is_monad.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

template <typename F>
struct [[nodiscard]] AndThen {
  F fun;

  AndThen(F f)
      : fun(std::move(f)) {
  }

  template <SomeFuture Future>
  auto Mapper() {
    return value::apply::AndThen<typename Future::ValueType, F>{std::move(fun)};
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    static_assert(value::IsMonad<ValueOf<Future>>);

    return lazy::thunks::Map{std::move(input), Mapper<Future>()};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

template <typename Mapper>
auto AndThen(Mapper mapper) {
  return pipe::AndThen{fun::Unitize(std::move(mapper))};
}

}  // namespace await::futures
