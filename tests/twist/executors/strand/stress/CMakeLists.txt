ProjectLog("Stress tests for tasks::Strand")

add_executable(await_stress_tests_strand main.cpp)
target_link_libraries(await_stress_tests_strand await)
