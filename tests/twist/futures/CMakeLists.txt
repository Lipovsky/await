ProjectLog("Stress tests for futures")

add_executable(await_stress_tests_futures main.cpp)
target_link_libraries(await_stress_tests_futures await)
