#include <wheels/test/framework.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>

#include <await/tasks/exe/thread_pool.hpp>
#include <await/tasks/exe/strand.hpp>

#include <await/futures/make/submit.hpp>

#include <await/await.hpp>

#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/first_of.hpp>

#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/box.hpp>

#include <await/futures/run/go.hpp>

#include <wheels/core/unit.hpp>

#include <atomic>
#include <chrono>

using namespace await;
using namespace std::chrono_literals;

using wheels::Unit;

//////////////////////////////////////////////////////////////////////

void StressTestPipeline() {
  tasks::ThreadPool pool{4};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    ++iter;

    size_t pipelines = 1 + iter % 3;

    std::atomic<size_t> counter1{0};
    std::atomic<size_t> counter2{0};
    std::atomic<size_t> counter3{0};

    for (size_t j = 0; j < pipelines; ++j) {
      auto f = futures::Submit(pool,
                               [&]() {
                                 ++counter1;
                               })
                   | futures::Apply([&]() { ++counter2;} )
                   | futures::Start()
                   | futures::Via(pool)
                   | futures::Apply([&]() { ++counter3; })
                   | futures::Start();

      futures::Submit(pool, [f = std::move(f)]() mutable {
        std::move(f).RequestCancel();
      }) | futures::Go();
    }

    pool.WaitIdle();

    ASSERT_TRUE(counter1.load() >= counter2.load());
    ASSERT_TRUE(counter2.load() >= counter3.load());

//    std::cout
//        << counter1.load() << " -> "
//        << counter2.load() << " -> "
//        << counter3.load() << std::endl;
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Cancellation) {
  TWIST_TEST(StressPipeline, 5s) {
    StressTestPipeline();
  }
}

RUN_ALL_TESTS()
