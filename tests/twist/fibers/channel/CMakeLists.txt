ProjectLog("Stress tests for fibers::Channel")

add_executable(await_stress_tests_channel main.cpp)
target_link_libraries(await_stress_tests_channel await)
