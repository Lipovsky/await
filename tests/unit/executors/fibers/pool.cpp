#include <await/tasks/exe/fibers/pool.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/submit.hpp>

#include <await/await.hpp>

#include <wheels/test/framework.hpp>

using namespace await;

TEST_SUITE(FiberPool) {
  SIMPLE_TEST(Yield) {
    tasks::fibers::Pool pool{4};

    static const size_t kYields = 7;

    size_t yields = 0;

    auto f = futures::Submit(pool, [&yields]() {
      for (size_t i = 0; i < kYields; ++i) {
        fibers::Yield();
        ++yields;
      }
    });

    Await((std::move(f)));

    ASSERT_EQ(yields, kYields);

    pool.Stop();
  }
}
