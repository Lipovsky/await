#include <await/futures/make/submit.hpp>
#include <await/futures/make/after.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/make/just.hpp>
#include <await/futures/make/never.hpp>
#include <await/futures/make/contract.hpp>

#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/map.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/unwrap.hpp>

#include <await/await.hpp>

#include <await/tasks/exe/manual.hpp>
#include <await/timers/impl/queue.hpp>

#include <wheels/test/framework.hpp>

#include <thread>

using namespace await;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

TEST_SUITE(FutureMake) {
  SIMPLE_TEST(Execute) {
    tasks::ManualExecutor manual;

    // Ok

    bool done = false;

    auto f = futures::Submit(manual, [&]() {
      done = true;
    });

    ~std::move(f);

    manual.Drain();

    ASSERT_TRUE(done);
  }

  SIMPLE_TEST(Just) {
    {
      // Just

      auto f = !futures::Just();

      ASSERT_TRUE(f.IsValid());
      ASSERT_TRUE(f.HasOutput());

      wheels::Unit unit = std::move(f) | futures::Unwrap();
      (void)unit;
    }

    {
      // Value

      auto f = !futures::Value<int64_t>(7);

      ASSERT_TRUE(f.HasOutput());
      int64_t value = std::move(f) | futures::Unwrap();

      ASSERT_EQ(value, 7);
    }

    {
      // Map

      auto f = futures::Just();

      bool done = false;

      std::move(f) |
          futures::Apply([&done]() {
            done = true;
          }) |
          futures::Start() |
          futures::Go();

      ASSERT_TRUE(done);
    }
  }

  SIMPLE_TEST(After) {
    timers::Queue timers;

    auto after = futures::After(timers.Delay(1s));

    Await(std::move(after));
  }

  SIMPLE_TEST(Never) {
#if !__has_feature(address_sanitizer)
    // Memory leak
    auto never = !futures::Never();
    ASSERT_FALSE(never.HasOutput());
#endif
  }

  SIMPLE_TEST(Contract) {
    auto [f, p] = futures::Contract<int>();

    ASSERT_FALSE(f.HasOutput());

    std::move(p).Set(1);

    ASSERT_TRUE(f.HasOutput());
    auto value = std::move(f) | futures::Unwrap();
    ASSERT_EQ(value, 1);
  }
}
