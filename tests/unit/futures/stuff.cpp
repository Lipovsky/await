#include <wheels/test/framework.hpp>
#include <wheels/test/util/cpu_timer.hpp>
#include <wheels/test/util/cpu_time_budget.hpp>

#include <await/futures/values/expected.hpp>

#include <await/futures/make/contract.hpp>
#include <await/futures/make/submit.hpp>

#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/or_else.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/handle.hpp>

#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/unwrap.hpp>
#include <await/futures/syntax/bang.hpp>

#include <await/await.hpp>

#include <await/tasks/exe/manual.hpp>
#include <await/tasks/exe/thread_pool.hpp>
#include <await/tasks/exe/strand.hpp>

#include <await/timers/impl/queue.hpp>

#include <thread>

using namespace await;

//////////////////////////////////////////////////////////////////////

static std::error_code TimeoutError() {
  return std::make_error_code(std::errc::timed_out);
}

//////////////////////////////////////////////////////////////////////

template <typename T>
using Result = tl::expected<T, std::error_code>;

//////////////////////////////////////////////////////////////////////

struct MoveOnly {
  explicit MoveOnly(std::string data_) : data(data_) {
  }

  MoveOnly(const MoveOnly& that) = delete;
  MoveOnly& operator=(const MoveOnly& that) = delete;

  MoveOnly(MoveOnly&& that) = default;

  std::string data;
};

struct NonDefaultConstructable {
  explicit NonDefaultConstructable(int v) : value(v) {
  }

  int value{0};
};

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Eager) {
//  SIMPLE_TEST(Invalid) {
//    auto f = futures::EagerFuture<std::string>::Invalid();
//    ASSERT_FALSE(f.IsValid());
//  }

  SIMPLE_TEST(SetValue) {
    auto [f, p] = futures::Contract<std::string>();

    ASSERT_FALSE(f.HasOutput());

    std::move(p).Set("Hello");

    ASSERT_TRUE(f.HasOutput());

    auto value = *std::move(f);
    ASSERT_EQ(value, "Hello");
  }

  SIMPLE_TEST(SetMoveOnlyValue) {
    auto [f, p] = futures::Contract<MoveOnly>();

    ASSERT_FALSE(f.HasOutput());

    std::move(p).Set(MoveOnly{"Hello"});

    ASSERT_TRUE(f.HasOutput());

    auto value = *std::move(f);
    ASSERT_EQ(value.data, "Hello");
  }

  SIMPLE_TEST(ExecuteValue) {
    tasks::ManualExecutor manual;

    auto f = !futures::Submit(manual, []() {
      return 7;
    });

    manual.RunNext();

    ASSERT_TRUE(f.HasOutput());

    auto value = *std::move(f);
    ASSERT_EQ(value, 7);
  }

//  SIMPLE_TEST(ExecuteError) {
//    tasks::ManualExecutor manual;
//
//    auto f = !futures::Execute(manual, []() {
//      throw std::runtime_error("Test");
//    });
//
//    manual.RunNext();
//
//    ASSERT_TRUE(f.HasOutput());
//
//    auto result = *std::move(f);
//
//    ASSERT_TRUE(result.Failed());
//    ASSERT_THROW(result.ThrowIfError(), std::runtime_error);
//  }

  SIMPLE_TEST(BlockingGet1) {
    auto [f, p] = futures::Contract<int>();

    std::move(p).Set(3);

    auto value = Await(std::move(f));
    ASSERT_EQ(value, 3);
  }

  SIMPLE_TEST(BlockingGet2) {
    auto [f, p] = futures::Contract<int>();

    std::thread producer([p = std::move(p)]() mutable {
      std::this_thread::sleep_for(2s);
      std::move(p).Set(17);
    });

    {
      wheels::test::CpuTimeBudgetGuard cpu_time_budget{256ms};

      auto value = Await(std::move(f));
      ASSERT_EQ(value, 17);
    }

    producer.join();
  }

  SIMPLE_TEST(Map1) {
    auto [f, p] = futures::Contract<int>();

    auto g = std::move(f) | futures::Apply([](int value) {
      return value + 1;
    }) | futures::Start();

    std::move(p).Set(3);

    auto value = *std::move(g);
    ASSERT_EQ(value, 4);
  }

  SIMPLE_TEST(Map2) {
    wheels::test::CpuTimeBudgetGuard cpu_time_budget{100ms};

    tasks::ThreadPool pool{4};

    auto f1 = !futures::Submit(pool, []() {
      std::this_thread::sleep_for(1s);
      return 42;
    });

    auto f2 = std::move(f1) | futures::Apply([](int value) {
      return value + 1;
    }) | futures::Start();

    ASSERT_EQ(Await(std::move(f2)), 43);

    pool.Stop();
  }

  SIMPLE_TEST(Map3) {
    wheels::test::CpuTimeBudgetGuard cpu_time_budget{100ms};

    tasks::ThreadPool pool{4};

    auto [f, p] = futures::Contract<int>();

    std::move(p).Set(11);

    auto f2 = std::move(f) | futures::Via(pool) | futures::Apply([&](int value) {
      ASSERT_TRUE(tasks::ThreadPool::Current() == &pool);
      return value + 1;
    }) | futures::Start();

    int value = Await(std::move(f2));

    ASSERT_EQ(value, 12);

    pool.Stop();
  }

  SIMPLE_TEST(Map4) {
    auto [f, p] = futures::Contract<Result<int>>();

    auto g = std::move(f) | futures::Map([](int /*value*/) -> int {
      FAIL_TEST("Skip continuation if error");
      return 7;  // Make compiler happy
    }) | futures::Start();

    std::move(p).Set(tl::unexpected(TimeoutError()));

    auto result = *std::move(g);
    ASSERT_FALSE(result);
  }

  // Map chaining

  SIMPLE_TEST(Pipeline) {
    tasks::ManualExecutor manual;

    bool done = false;

    futures::Submit(manual,
                    []() {
                      return 1;
                    })
         | futures::Apply([](int value) {
          return value + 1;
        })
         | futures::Apply([](int value) {
          return value + 2;
        })
         | futures::Apply([](int value) {
          return value + 3;
        })
         | futures::Start()
         | futures::Apply([&done](int value) {
          std::cout << "Value = " << value << std::endl;
          done = true;
        })
         | futures::Start()
         | futures::Go();

     manual.Drain();
    //ASSERT_EQ(tasks, 5);

    ASSERT_TRUE(done);
  }

  // Map chaining

  SIMPLE_TEST(PipelineError) {
    tasks::ManualExecutor manual;

    bool done = false;

    futures::Submit(manual,
                    []() {
                      return 1;
                    })
         | futures::Apply([](int) -> Result<int> {
          return tl::unexpected(TimeoutError());
        })
         | futures::Map([](int) -> int {
          std::abort();  // Skipped
        })
         | futures::Map([](int) -> int {
          std::abort();  // Skipped
        })
         | futures::Start()
         | futures::Apply([&done](Result<int> result) {
          ASSERT_FALSE(result);
          done = true;
        })
         | futures::Start() | futures::Go();

    manual.Drain();
    // ASSERT_EQ(tasks, 5);

    ASSERT_TRUE(done);
  }

  // Map + OrElse

  SIMPLE_TEST(PipelineRecover) {
    wheels::test::CpuTimeBudgetGuard cpu_time_budget{100ms};

    tasks::ManualExecutor manual;

    bool done = false;

    futures::Submit(manual,
                    []() {
                      return 1;
                    })
         | futures::Apply([](int) -> Result<int> {
          return tl::unexpected(TimeoutError());
        })
         | futures::Map([](int) -> int {
          std::abort();  // Skipped
        })
         | futures::OrElse([](std::error_code) -> Result<int> {
          return 7;
        })
         | futures::Map([](int value) {
          return value + 1;
        })
         | futures::Start()
         | futures::Apply([&done](Result<int> result) {
          ASSERT_EQ(result.value(), 8);
          done = true;
        })
         | futures::Start() | futures::Go();

    manual.Drain();
    // ASSERT_EQ(tasks, 6);

    ASSERT_TRUE(done);
  }

  // Via + Map

  SIMPLE_TEST(Via) {
    tasks::ManualExecutor manual1;
    tasks::ManualExecutor manual2;

    auto [f, p] = futures::Contract<int>();

    int step = 0;

    auto f2 = std::move(f)
                   | futures::Via(manual1)
                   | futures::Apply([&](int value) {
                    step = 1;
                    return value + 1;
                  })
                   | futures::Apply([&](int value) {
                    step = 2;
                    return value + 2;
                  })
                   | futures::Via(manual2)
                   | futures::Apply([&](int value) {
                    step = 3;
                    return value + 3;
                  }) | futures::Start();

    // Launch pipeline
    std::move(p).Set(0);

    // ASSERT_EQ(manual1.Drain(), 2);
    manual1.Drain();
    ASSERT_EQ(step, 2);
    // ASSERT_EQ(manual2.Drain(), 1);
    manual2.Drain();
    ASSERT_EQ(step, 3);

    auto f3 = std::move(f2)
                   | futures::Apply([&](int value) {
                    step = 4;
                    return value + 4;
                  })
                   | futures::Via(manual1)
                   | futures::Apply([&](int value) {
                    step = 5;
                    return value + 5;
                  }) | futures::Start();

    // ASSERT_EQ(manual2.Drain(), 1);
    manual2.Drain();
    ASSERT_EQ(step, 4);

    // ASSERT_EQ(manual1.Drain(), 1);
    manual1.Drain();
    ASSERT_EQ(step, 5);

    ASSERT_TRUE(f3.HasOutput());

    auto value = *std::move(f3);

    ASSERT_EQ(value, 1 + 2 + 3 + 4 + 5);
  }

  SIMPLE_TEST(OnCancel) {
    auto [f, p] = futures::Contract<int>();

    bool handler_invoked = false;

    auto g = std::move(f) | futures::OnCancel([&]() {
      handler_invoked = true;
    }) | futures::Start();

    std::move(p).Cancel();

    ASSERT_TRUE(g.IsCancelled());
    ASSERT_TRUE(handler_invoked);
  }
}
