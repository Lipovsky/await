#include <wheels/test/framework.hpp>

#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sync/wait_group.hpp>

#include <await/futures/values/expected.hpp>

#include <await/tasks/exe/manual.hpp>
#include <await/tasks/exe/fibers/pool.hpp>
#include <await/tasks/exe/thread_pool.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/make/just.hpp>

#include <await/futures/combine/seq/monad.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/box.hpp>
#include <await/futures/combine/seq/sched.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/combine/seq/clone.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/unwrap.hpp>

#include <await/await.hpp>

#include <carry/new.hpp>
#include <carry/current.hpp>

#include <exception>
#include <vector>

using namespace await;

TEST_SUITE(FutureSeq) {
  SIMPLE_TEST(JustWorks) {
    auto l = futures::Just() | futures::Apply([]() {
        return 42;
    });

    auto value = *std::move(l);

    ASSERT_EQ(value, 42);
  }

  SIMPLE_TEST(IsValid) {
    auto l2 = futures::Value(7);

    auto value = *std::move(l2);

    ASSERT_EQ(value, 7);
  }

  SIMPLE_TEST(MoveCtor) {
    auto l1 = futures::Value(7);

    auto l2 = std::move(l1);

    auto value = *std::move(l2);

    ASSERT_EQ(value, 7);
  }

  SIMPLE_TEST(MoveAssignmentOperator) {
    auto l1 = futures::Value(7) | futures::Box();

    auto l2 = futures::Value(8) | futures::Box();

    l2 = std::move(l1);

    auto value = *std::move(l2);

    ASSERT_EQ(value, 7);
  }

  SIMPLE_TEST(ToEager) {
    auto l = futures::Just() |
             futures::Apply([]() { return 42; });

    auto f = std::move(l) | futures::Start();

    ASSERT_TRUE(f.HasOutput());
    ASSERT_EQ((*std::move(f)), 42);
  }

  SIMPLE_TEST(ToEagerToLazy) {
    // Lazy<Thunk>
    auto l = futures::Invoke([]() {
        return 42;
    });

    // Eager<int>
    auto f = std::move(l) | futures::Start();

    ASSERT_TRUE(f.HasOutput());

    futures::BoxedFuture<int> boxed = std::move(f) | futures::Box();
  }

//  SIMPLE_TEST(OperatorEager) {
//    futures::EagerFuture<int> f = futures::Value(17);
//
//    ASSERT_TRUE(f.HasOutput());
//  }

//  SIMPLE_TEST(OperatorLazy) {
//    auto f = !futures::Invoke([]() {
//        return 42;
//      });
//
//    futures::BoxedFuture<int> l = std::move(f);
//  }

  SIMPLE_TEST(ActuallyLazy) {
    tasks::ManualExecutor manual;

    // Prepare
    auto l = futures::Just() |
             futures::Via(manual) |
             futures::Apply([]() { return 42; });

    // Empty
    ASSERT_EQ(manual.Drain(), 0);

    // Start
    auto f = std::move(l) | futures::Start();

    // Run
    ASSERT_EQ(manual.Drain(), 1);

    // Check
    ASSERT_EQ(*std::move(f), 42);
  }

  // TODO
//  SIMPLE_TEST(Context) {
//    auto ctx = carry::New().SetString("key", "test").Done();
//
//    auto l = futures::Just() |
//             futures::With(std::move(ctx)) |
//             futures::Map([]() {
//               auto ctx = carry::Current();
//               ASSERT_EQ(ctx.GetString("key"), "test");
//               return 1;
//             });
//
//    {
//      auto l_ctx = l.UserContext();
//      ASSERT_EQ(l_ctx.GetString("key"), "test");
//    }
//
//    auto result = *std::move(l);
//
//    ASSERT_EQ(result.ExpectValue(), 1);
//  }

  SIMPLE_TEST(Apply) {
    auto l = futures::Value(1) |
             futures::Apply([](int value) { return value + 1; });

    auto value = *std::move(l);

    ASSERT_EQ(value, 2);
  }

  SIMPLE_TEST(UnitizeMapper) {
    auto l = futures::Just()
             | futures::Apply([]() {})
             | futures::Apply([] { return 7; });

    ASSERT_EQ(*std::move(l), 7);
  }

  SIMPLE_TEST(Mappers) {
    using Result = tl::expected<int, int>;

    {
      // Map

      auto l = futures::Value(Result(1)) |
               futures::Map([](int value) { return value + 1; });

      auto result = *std::move(l);

      ASSERT_TRUE(result);
      ASSERT_EQ(*result, 2);
    }

    {
      // AndThen

      auto l = futures::Value(Result(1)) | futures::AndThen([](int value) -> Result {
        return value + 2;
      });

      auto result = *std::move(l);
      ASSERT_TRUE(result);
      ASSERT_EQ(*result, 3);
    }

    {
      // Apply

      auto l = futures::Value(Result(7)) | futures::Apply([](Result input) {
        return input;
      });

      auto result = *std::move(l);

      ASSERT_TRUE(result);
      ASSERT_EQ(*result, 7);
    }
  }

  std::error_code TestError() {
    return std::make_error_code(std::errc::io_error);
  }

  SIMPLE_TEST(OrElse) {
    using Result = tl::expected<int, std::error_code>;

    auto fail = []() -> Result {
      return tl::unexpected(TestError());
    };

    {
      auto l = futures::Value(Result(7)) |
               futures::OrElse([](std::error_code) -> Result {
                 return 2;
               });

      auto result = *std::move(l);

      ASSERT_TRUE(result);
      ASSERT_EQ(*result, 7);
    }

    {
      auto l = futures::Value(fail()) |
          futures::OrElse([](std::error_code) -> Result {
                 return 2;
               });

      auto result = *std::move(l);

      ASSERT_TRUE(result);
      ASSERT_EQ(*result, 2);
    }

    {
      auto l = futures::Value(fail()) |
          futures::Map([](int value) {
            std::abort();
            return value + 1;  // Ignored
          }) |
          futures::OrElse([](std::error_code) -> Result {
            return 2;
          });

      auto result = *std::move(l);

      ASSERT_TRUE(result);
      ASSERT_EQ(*result, 2);
    }
  }

  SIMPLE_TEST(OnComplete) {
    auto output = futures::Value(7) |
             futures::OnComplete([](const int v) {
               ASSERT_EQ(v, 7);
             }) |
             futures::Unwrap();

    ASSERT_EQ(output, 7);
  }

  SIMPLE_TEST(OnCancel) {

  }

  SIMPLE_TEST(Anyway) {
    {
      // Output

      tasks::ManualExecutor manual;

      bool ok = false;

      futures::Submit(manual, []() {
        return 3;
      }) |
          futures::Anyway([&]() { ok = true; }) |
          futures::Go();

      manual.Drain();

      ASSERT_TRUE(ok);
    }

    {
      // Cancel

      tasks::ManualExecutor manual;

      bool ok = false;

      auto l = futures::Submit(manual,
                               []() {
                                 return 3;
                               }) |
               futures::Anyway([&]() { ok = true; });

      auto f = std::move(l) | futures::Start();
      f.RequestCancel();

      manual.Drain();

      ASSERT_TRUE(ok);
    }
  }

  SIMPLE_TEST(BoxedLazy) {
    auto l = futures::Value(1);

    futures::BoxedFuture<int> lf = std::move(l) | futures::Box();

    int value = *std::move(lf);

    ASSERT_EQ(value, 1);
  }

  SIMPLE_TEST(Go) {
    tasks::ManualExecutor manual;

    int output = 0;

    futures::Just() |
        futures::Via(manual) |
        futures::Apply([]() { return 1; }) |
        futures::Apply([&](int value) {
          output = value;
          return true;
        }) |
        futures::Go();

    ASSERT_EQ(output, 0);

    manual.Drain();

    ASSERT_EQ(output, 1);
  }

  SIMPLE_TEST(WaitGroup) {
    tasks::fibers::Pool pool{4};

    auto f = futures::Submit(pool,
                             []() {
                               fibers::WaitGroup wg;

                               wg.Add(futures::Spawn([]() {
                                 while (true) {
                                   fibers::Yield();
                                 }
                               }));

                               wg.Wait();

                               FAIL_TEST("Not cancelled");
                             }) | futures::Start();

    std::move(f).RequestCancel();

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(BangOperator) {
    tasks::ManualExecutor manual;

    auto lazy = futures::Value(1) |
                futures::Via(manual) |
                futures::Apply([](int value) {
                  return value + 1;
                });

    ASSERT_EQ(manual.Drain(), 0);

    // Bang!
    auto future = std::move(lazy) |
                  futures::Apply([](int value) { return value + 1; }) |
                  futures::Start();

    ASSERT_TRUE(manual.Drain() > 0);

    ASSERT_TRUE(future.HasOutput());

    auto value = std::move(future) | futures::Unwrap();

    ASSERT_EQ(value, 3);
  }

  SIMPLE_TEST(GoOperator) {
    tasks::ManualExecutor manual;

    futures::Submit(manual, []() {
      return 1;
    }) |
        futures::Apply([](int value) {
          std::cout << "Value = " << value << std::endl;
        }) |
        futures::Go();

    ASSERT_EQ(manual.Drain(), 2);
  }

  SIMPLE_TEST(Yield) {
    tasks::pools::fast::ThreadPool pool{4};

    futures::Submit(pool, []() {}) | futures::Yield() | futures::Apply([](){}) | futures::Go();

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(Flatten) {
    auto exe = futures::Invoke([]() {
      return futures::Spawn([]() {
        return 7;
      });
    });

    auto flat = std::move(exe) | futures::Flatten();

    auto value = *std::move(flat);

    ASSERT_EQ(value, 7);
  }

  SIMPLE_TEST(ErrorFlatMap) {
    using Result = tl::expected<int, std::error_code>;

    auto fail = []() -> Result {
      return tl::unexpected(TestError());
    };

    auto err = futures::Value<Result>(fail());

    auto apply = std::move(err) | futures::Apply([&](Result) {
      return futures::Value<Result>(fail());
    });

    auto f = std::move(apply) |
             futures::Flatten() |
             futures::Map([](int) {
               std::abort();  // Unexpected
             });

    auto result = Await(std::move(f));

    ASSERT_FALSE(result);
  }

  SIMPLE_TEST(ExecuteInline) {
    bool done = false;

    auto exe = futures::Invoke([&]() { done = true; });

    ASSERT_FALSE(done);

    auto f = std::move(exe) | futures::Start();

    ASSERT_TRUE(done);
  }

//  SIMPLE_TEST(EagerOperatorBox) {
//    futures::EagerFuture<int> f = futures::Invoke([](){ return 1; });
//    {
//      futures::Future<int> l = std::move(f);
//    }
//  }

  SIMPLE_TEST(FlatLifetime) {
    tasks::fibers::Pool pool{4};

    auto l = futures::Submit(pool,
                             []() {
                               std::string data = "FlatLifetime";

                               return futures::Spawn([data]() {
                                        std::cout << data << std::endl;
                                      }) |
                                      futures::Box();
                             }) | futures::Flatten();

    Await(std::move(l));

    pool.Stop();
  }

  SIMPLE_TEST(AutoBoxing) {
    futures::BoxedFuture<int> f = futures::Value(7);
  }

  SIMPLE_TEST(Fork) {
    tasks::ThreadPool pool{2};

    auto [l, r] = futures::Submit(pool,
                                  [] {
                                    return 42;
                                  }) | futures::Clone();

    ASSERT_EQ(Await(std::move(l)), 42);
    ASSERT_EQ(Await(std::move(r)), 42);

    pool.Stop();
  }
}
