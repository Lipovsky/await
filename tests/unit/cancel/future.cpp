#include <wheels/test/framework.hpp>

#include <await/futures/values/optional.hpp>

#include <await/futures/make/contract.hpp>
#include <await/futures/make/submit.hpp>
#include <await/futures/make/never.hpp>
#include <await/futures/make/bomb.hpp>

#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/join.hpp>
#include <await/futures/combine/par/interrupt.hpp>

#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/clone.hpp>
#include <await/futures/combine/seq/flatten.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/sequence.hpp>

#include <await/tasks/exe/thread_pool.hpp>
#include <await/tasks/exe/inline.hpp>
#include <await/tasks/exe/manual.hpp>
#include <await/tasks/exe/fibers/manual.hpp>

#include <await/tasks/curr/checkpoint.hpp>

#include <await/cancel/current.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/await.hpp>

#include <thread>

using namespace await;

//////////////////////////////////////////////////////////////////////

TEST_SUITE(CancelFutures) {
  SIMPLE_TEST(PromiseCancel) {
    auto [f, p] = futures::Contract<int>();

    auto g = std::move(f) | futures::Apply([](int value) {
      std::abort();
      return value + 1;
    }) | futures::Start();

    auto h = std::move(g) | futures::Apply([](int value) {
      return value + 1;
    }) | futures::Start();

    std::move(p).Cancel();

    ASSERT_TRUE(h.IsCancelled());
  }

  SIMPLE_TEST(WaitResult) {
    auto [f, p] = futures::Contract<int>();

    std::thread producer([p = std::move(p)]() mutable {
      std::move(p).Cancel();
    });

    ASSERT_THROW(Await(std::move(f)), tasks::CancelledException);

    producer.join();
  }

  SIMPLE_TEST(FutureRequestCancel) {
    auto [f, p] = futures::Contract<int>();

    auto g = std::move(f) | futures::Apply([](int value) {
      std::abort();
      return value + 1;
    }) | futures::Apply([](int value) {
      return value + 1;
    }) | futures::Start();

    std::move(g).RequestCancel();

    p.CancelSubscribe(tasks::Inline(), []() {
      std::cout << "Cancel requested!" << std::endl;
    });

    ASSERT_TRUE(p.CancelRequested());

    std::move(p).Cancel();
  }

  struct Widget {
    ~Widget() {
      std::cout << "~Widget" << std::endl;
    }
  };

  SIMPLE_TEST(DropCancelHandlers) {
    tasks::fibers::ManualExecutor manual;

    auto [f, p] = futures::Contract<int>();

    p.CancelSubscribe(manual, []() {
      std::abort();
    });

    std::move(p).Set(7);

    std::move(f).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(BeforeMap) {
    tasks::ManualExecutor manual;

    auto f = futures::Submit(manual, []() {}) |
             futures::Apply([]() {}) |
             futures::Apply([]() { FAIL_TEST("Running"); }) |
             futures::Start();

    manual.RunAtMost(1);

    std::move(f).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(FirstOf) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2));

    std::move(p2).Set(17);

    ASSERT_TRUE(first_of.HasOutput());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(FirstOfMap) {
    tasks::fibers::ManualExecutor manual;

    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto first_of = futures::FirstOf(std::move(f1), std::move(f2));

    auto f = std::move(first_of) | futures::Via(manual) | futures::Apply([](int value) {
      tasks::curr::Checkpoint();
      std::cout << "Map!" << std::endl;
      return value;
    }) | futures::Start();

    std::move(p2).Set(17);

    manual.Drain();

    ASSERT_TRUE(f.HasOutput());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(All) {
    using OptInt = std::optional<int>;

    auto [f1, p1] = futures::Contract<OptInt>();
    auto [f2, p2] = futures::Contract<OptInt>();

    auto all = !futures::All(std::move(f1), std::move(f2));

    std::move(p2).Set(std::nullopt);

    ASSERT_TRUE(all.HasOutput());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(Join) {
    using OptInt = std::optional<int>;

    auto [f1, p1] = futures::Contract<OptInt>();
    auto [f2, p2] = futures::Contract<OptInt>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    std::move(p2).Set(std::nullopt);

    ASSERT_TRUE(join.HasOutput());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(Interrupt) {
    auto [f, p] = futures::Contract<int>();

    p.CancelSubscribe(tasks::Inline(), []() {
      std::cout << "Interrupted!" << std::endl;
    });

    // Interrupt
    auto [fi, pi] = futures::Contract<wheels::Unit>();

    auto g = !futures::Interrupt(std::move(f), std::move(fi));

    std::move(pi).Set(wheels::Unit{});

    ASSERT_TRUE(p.CancelRequested());
  }

  SIMPLE_TEST(Await1) {
    tasks::fibers::ManualExecutor manual;

    auto [f, p] = futures::Contract<int>();

    auto g = !futures::Submit(manual, [f = std::move(f)]() mutable {
      Await(std::move(f));
    });

    manual.Drain();

    ASSERT_TRUE(!g.HasOutput() && !g.IsCancelled());

    std::move(p).Cancel();

    manual.Drain();

    ASSERT_TRUE(g.IsCancelled());
  }

  SIMPLE_TEST(Await2) {
    tasks::fibers::ManualExecutor manual;

    futures::Submit(manual, [&]() {
      {
        std::cout << "Step 1" << std::endl;

        auto f = futures::Submit(manual, []() -> int {
          return 1;
        });
        std::cout << Await(std::move(f)) << std::endl;
      }  // <-- state for `f` has been destroyed

      {
        std::cout << "Step 2" << std::endl;

        auto f = futures::Submit(manual, []() -> int {
          return 2;
        });
        std::cout << Await(std::move(f)) << std::endl;
      }  // <-- state for `f` has been destroyed
    }) | futures::Detach();

    manual.Drain();
  }

  SIMPLE_TEST(ThreadAwait) {
    tasks::ThreadPool pool{4};

    auto f = !futures::Submit(pool, []() {
      auto inner = futures::Spawn([]() {
        while (true) {
          tasks::curr::Checkpoint();
        }
      });

      Await(std::move(inner));
    });

    std::move(f).RequestCancel();

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(PromiseDtor) {
    tasks::fibers::ManualExecutor manual;

    auto [f, p] = futures::Contract<int>();

    auto g = !futures::Submit(manual, [p = std::move(p)]() {
      while (true) {
        std::cout << "Hi!" << std::endl;
        fibers::Yield();
      }
    });

    manual.RunAtMost(7);

    std::move(g).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(Flatten) {
    tasks::ManualExecutor manual;

    auto f = futures::Submit(manual, [&]() {
      return futures::Submit(manual, []() {
        while (true) {
          tasks::curr::Checkpoint();
        }
      });
    });

    auto g = std::move(f) | futures::Flatten() | futures::Start();

    std::move(g).RequestCancel();

    manual.Drain();
  }

//  SIMPLE_TEST(CancelOnMoveAssignment) {
//    tasks::fibers::ManualExecutor manual;
//
//    auto f = !futures::Execute(manual, []() -> int {
//      while (true) {
//        fibers::Yield();
//      }
//    });
//
//    // Cancel first task
//    f = !futures::Execute(manual, []() {
//      return 7;
//    });
//
//    manual.Drain();
//  }

  SIMPLE_TEST(Fork) {
    auto [input, p] = futures::Contract<int>();

    auto [left, right] = std::move(input) | futures::Clone();

    std::move(left).RequestCancel();

    auto righte = std::move(right) | futures::Start();

    ASSERT_TRUE(p.CancelRequested());
    std::move(p).Cancel();

    {
      ASSERT_TRUE(righte.IsCancelled());
    }
  }

  SIMPLE_TEST(ExecuteFork) {
    tasks::fibers::ManualExecutor manual;

    auto f = futures::Submit(manual,
                             []() {
                               while (true) {
                                 fibers::Yield();
                               }
                             }) | futures::Apply([](){}) |
             futures::Start() |
             futures::Apply([](){}) |
             futures::Start();

    auto [l, r] = std::move(f) | futures::Clone();

    std::move(r).RequestCancel();

    auto le = std::move(l) | futures::Start();

    manual.Drain();

    ASSERT_TRUE(le.IsCancelled());
  }

  SIMPLE_TEST(GoFork) {
    tasks::fibers::ManualExecutor manual;

    auto f = futures::Submit(manual,
                             []() {
                               while (true) {
                                 fibers::Yield();
                               }
                             }) | futures::Apply([](){}) |
             futures::Start() |
             futures::Apply([](){}) |
             futures::Start();

    auto [l, r] = std::move(f) | futures::Clone();

    std::move(l) | futures::Apply([](){}) | futures::Go();

    manual.RunAtMost(6);

    std::move(r).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(DropAsLazy) {
    tasks::fibers::ManualExecutor manual;

    auto f = !futures::Submit(manual, []() {
      while (true) {
        fibers::Yield();
      }
    });

    f.IAmEager();

    {
      auto l = std::move(f) | futures::Apply([](){});
      // f.IAmEager();
    }

    manual.Drain();
  }

  SIMPLE_TEST(DropBoxed) {
    tasks::fibers::ManualExecutor manual;

    auto f = !futures::Submit(manual, []() {
      while (true) {
        fibers::Yield();
      }
    });

    f.IAmEager();

    {
      auto l = std::move(f) | futures::Box();
    }

    manual.Drain();
  }

  SIMPLE_TEST(AllReset) {
    tasks::fibers::ManualExecutor manual;

    auto f = !futures::Submit(manual, []() {
      while (true) {
        fibers::Yield();
      }
    });

    auto g = futures::Submit(manual, []() {
      auto h = futures::Spawn([]() {
        return 42;
      });
      ASSERT_FALSE(cancel::Current().HasLinks());
      Await(std::move(h));
    });

    auto all = !futures::All(std::move(f), std::move(g));

    size_t steps = manual.RunAtMost(11);
    ASSERT_EQ(steps, 11);

    std::move(all).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(FirstOfReset) {
    tasks::fibers::ManualExecutor manual;

    auto f = !futures::Submit(manual, []() -> std::optional<int> {
      while (true) {
        fibers::Yield();
      }
      return 7;
    });

    auto g = futures::Submit(manual, []() {
      auto h = futures::Spawn([]() -> std::optional<int> {
        return std::nullopt;
      });
      return Await(std::move(h));
    });

    auto first = !futures::FirstOf(std::move(f), std::move(g));

    size_t tasks = manual.RunAtMost(11);

    std::cout << "tasks = " << tasks << std::endl;
    ASSERT_EQ(tasks, 11);

    std::move(first).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(ResetCancelTokenAfterEagerFuture) {
    tasks::fibers::ManualExecutor manual;

    auto f = !futures::Submit(manual, []() {});

    auto g = std::move(f) | futures::Apply([]() {
      ASSERT_FALSE(cancel::Current().HasLinks());

      auto h = !futures::Spawn([](){});
      Await(std::move(h));
    }) | futures::Box();

    ~std::move(g);

    manual.Drain();
  }

  SIMPLE_TEST(Never) {
    tasks::fibers::ManualExecutor manual;

    bool cancelled = false;

    auto f = futures::Submit(manual,
                             [&]() {
                               wheels::Defer defer([&] {
                                 cancelled = true;
                               });

                               auto never = futures::Never();
                               Await(std::move(never));
                             }) | futures::Start();

    manual.Drain();

    f.RequestCancel();

    manual.Drain();

    ASSERT_TRUE(cancelled);
  }

  SIMPLE_TEST(Sequence) {
    tasks::ManualExecutor manual;

    auto seq = futures::Submit(manual, []{}) >> futures::Bomb();

    auto f = !std::move(seq);

    std::move(f).RequestCancel();

    size_t tasks = manual.Drain();
    ASSERT_EQ(tasks, 1);
  }
}
