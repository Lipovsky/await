#include <wheels/test/framework.hpp>

#include <await/futures/values/optional.hpp>

#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sync/wait_group.hpp>

#include <await/tasks/exe/fibers/pool.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/syntax/bang.hpp>

#include <await/await.hpp>

#include <exception>

using namespace await;

TEST_SUITE(CancelWaitGroup) {
  SIMPLE_TEST(JustWait) {
    tasks::fibers::Pool pool{4};

    auto join = futures::Submit(pool, []() {
      fibers::WaitGroup wg;

      for (size_t i = 0; i < 4; ++i) {
        wg.Add(futures::Spawn([]() {
          for (size_t j = 0; j < 3; ++j) {
            fibers::Yield();
          }
          return 42;
        }));
      }

      wg.Wait();
    });

    Await(std::move(join));

    pool.Stop();
  }

  SIMPLE_TEST(RequestCancel) {
    tasks::fibers::Pool pool{4};

    auto join = futures::Submit(pool, []() {
      fibers::WaitGroup wg;

      wg.Add(futures::Spawn([]() {
        while (true) {
          fibers::Yield();
        }
      }));

      wg.RequestCancel();
      wg.Wait();
    });

    Await(std::move(join));

    pool.Stop();
  }

  SIMPLE_TEST(SiblingError) {
    tasks::fibers::Pool pool{4};

    auto join = futures::Submit(pool, []() {
      fibers::WaitGroup wg;

      wg.Add(futures::Spawn([]() {
        while (true) {
          fibers::Yield();
        }
      }));

      wg.Add(futures::Spawn([]() -> std::optional<int> {
        return std::nullopt;
      }));

      wg.Wait();
    });

    Await(std::move(join));

    pool.Stop();
  }

  SIMPLE_TEST(PropagateCancel) {
    tasks::fibers::Pool pool{4};

    auto f = !futures::Submit(pool, []() {
      fibers::WaitGroup wg;

      wg.Add(futures::Spawn([]() {
        while (true) {
          fibers::Yield();
        }
      }));

      wg.Wait();

      FAIL_TEST("Not cancelled");
    });

    std::move(f).RequestCancel();

    pool.WaitIdle();
    pool.Stop();
  }
}
