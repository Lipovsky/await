#include <await/tasks/exe/fibers/pool.hpp>
#include <await/timers/impl/queue.hpp>

#include <await/futures/values/expected.hpp>

// fibers::Yield
#include <await/fibers/sched/yield.hpp>
// fibers::AmIFiber
#include <await/fibers/sched/self.hpp>
#include <await/fibers/sync/wait_group.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/make/never.hpp>
#include <await/futures/combine/seq/timeout.hpp>
#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/syntax/bang.hpp>

#include <await/await.hpp>

// Cancellation checkpoint
#include <await/tasks/curr/checkpoint.hpp>

// wheels::Defer
#include <wheels/core/defer.hpp>

#include <iostream>

using namespace await;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

void FirstOfExample() {
  std::cout << "FirstOf (Structured Concurrency) Example" << std::endl;

  tasks::fibers::Pool pool{/*threads=*/4};

  {
    // Never completes
    auto f = futures::Submit(pool, []() -> int {
      assert(fibers::AmIFiber());

      wheels::Defer log([] {
        std::cout << "Cancelled" << std::endl;
      });

      while (true) {
        std::cout << "Keep running..." << std::endl;
        fibers::Yield();  // Checkpoint
      }

      std::abort();  // Unreachable
    });

    auto g = futures::Submit(pool, [] {
      return 42;
    });

    // Structured concurrency!
    auto first = futures::FirstOf(std::move(f), std::move(g));

    int value = Await(std::move(first));
    std::cout << "FirstOf -> " << value << std::endl;
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WithTimeoutExample() {
  std::cout << "WithTimeout Example" << std::endl;

  tasks::fibers::Pool pool{4};
  timers::Queue timers;

  std::atomic<int> iters = 0;

  auto loop = futures::Submit(pool,
                              [&iters] {
                                wheels::Defer log([] {
                                  std::cout << "Cancelled" << std::endl;
                                });

                                while (true) {
                                  ++iters;
                                  fibers::Yield();
                                }
                              }) | futures::WithTimeout(timers.Delay(1s));

  Await(std::move(loop));

  std::cout << "Iterations made: " << iters.load() << std::endl;

  timers.Stop();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void ExplicitCancelExample() {
  std::cout << "ExplicitCancel Example" << std::endl;

  tasks::fibers::Pool pool{/*threads=*/4};

  {
    auto f = !futures::Submit(pool, [] {
      wheels::Defer log([] {
        std::cout << "Cancelled" << std::endl;
      });

      while (true) {
        std::cout << "Keep running..." << std::endl;
        tasks::curr::Checkpoint();  // <- Explicit cancellation checkpoint
      }

      std::abort();  // Unreachable
    });

    f.IAmEager();

    // Consume future by issuing cancellation request
    std::move(f).RequestCancel();
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void FutureDtorExample() {
  std::cout << "FutureDtor Example" << std::endl;

  tasks::fibers::Pool pool{/*threads=*/4};

  {
    auto f = !futures::Submit(pool, [] {
      wheels::Defer log([] {
        std::cout << "Cancelled" << std::endl;
      });

      while (true) {
        std::cout << "Keep running..." << std::endl;
        tasks::curr::Checkpoint();  // <- Explicit cancellation checkpoint
      }

      std::abort();  // Unreachable
    });

    f.IAmEager();

  }  // <- future will be automatically cancelled here

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void AwaitExample() {
  std::cout << "Await Example" << std::endl;

  tasks::fibers::Pool pool{/*threads=*/4};

  auto root = !futures::Submit(pool, [] {
    assert(fibers::AmIFiber());

    wheels::Defer log([] {
      std::cout << "Parent cancelled" << std::endl;
    });

    auto while_true = !futures::Spawn([]() -> int {
      wheels::Defer log([] {
        std::cout << "Inner cancelled" << std::endl;
      });

      while (true) {
        std::cout << "Keep running..." << std::endl;
        fibers::Yield();
      }

      std::abort();  // Unreachable
    });

    // Doomed
    Await(std::move(while_true));

    std::abort();  // Unreachable
  });

  std::move(root).RequestCancel();

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void NeverExample() {
  std::cout << "Never Example" << std::endl;

  tasks::fibers::Pool pool{/*threads=*/4};
  timers::Queue timers;

  auto f = futures::Submit(pool,
                           [] {
                             wheels::Defer log([] {
                               std::cout << "Cancelled" << std::endl;
                             });

                             Await(futures::Never());
                           }) | futures::WithTimeout(timers.Delay(1s));

  auto ok = Await(std::move(f));
  assert(!ok);

  std::cout << "Completed" << std::endl;

  timers.Stop();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WaitGroupExample1() {
  std::cout << "WaitGroup Example #1" << std::endl;

  tasks::fibers::Pool pool{4};

  auto root = futures::Submit(pool, [] {
    fibers::WaitGroup wg;

    wheels::Defer log([] {
      std::cout << "Parent cancelled" << std::endl;
    });

    wg.Add(futures::Spawn([] {
      wheels::Defer log([] {
        std::cout << "Child cancelled" << std::endl;
      });

      while (true) {
        fibers::Yield();
      }
    }));

    wg.RequestCancel();
    wg.Wait();
  });

  Await(std::move(root));

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

auto Timeout() {
  return std::make_error_code(std::errc::timed_out);
}

void WaitGroupExample2() {
  std::cout << "WaitGroup Example #2" << std::endl;

  tasks::fibers::Pool pool{4};

  using Result = tl::expected<int, std::error_code>;

  auto root = futures::Submit(pool, [] {
    fibers::WaitGroup wg;

    wg.Add(futures::Spawn([] {
      wheels::Defer log([] {
        std::cout << "Sibling cancelled" << std::endl;
      });

      while (true) {
        fibers::Yield();
      }
    }));

    wg.Add(futures::Spawn([]() -> Result {
      return tl::unexpected(Timeout());
    }));

    wg.Wait();
  });

  Await(std::move(root));

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WaitGroupExample3() {
  std::cout << "WaitGroup Example #3" << std::endl;

  tasks::fibers::Pool pool{4};

  auto root = !futures::Submit(pool, [] {
    fibers::WaitGroup wg;

    wheels::Defer log([] {
      std::cout << "Parent cancelled" << std::endl;
    });

    wg.Add(futures::Spawn([] {
      wheels::Defer log([] {
        std::cout << "Child cancelled!" << std::endl;
      });

      while (true) {
        fibers::Yield();
      }
    }));

    wg.Wait();
  });

  std::move(root).RequestCancel();

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  FirstOfExample();
  WithTimeoutExample();
  ExplicitCancelExample();
  FutureDtorExample();
  AwaitExample();
  NeverExample();

  WaitGroupExample1();
  WaitGroupExample2();
  WaitGroupExample3();

  return 0;
}
