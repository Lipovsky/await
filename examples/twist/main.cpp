#include <await/tasks/exe/fibers/pool.hpp>
#include <await/tasks/exe/strand.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/run/go.hpp>

#include <twist/rt/run.hpp>

#include <fmt/core.h>

using namespace await;

void TwistedExample() {
  tasks::fibers::Pool pool{4};
  tasks::Strand strand{pool};

  size_t cs = 0;

  for (int i = 0; i < 7; ++i) {
    futures::Submit(pool,
                    [] {
                      for (size_t i = 0; i < 3; ++i) {
                        fibers::Yield();
                      }
                    })
        | futures::Via(strand)
        | futures::Apply([&cs](wheels::Unit) {
          ++cs;
        })
        | futures::Go();
  }

  pool.WaitIdle();
  pool.Stop();

  fmt::println("cs = {}", cs);
}

int main() {
  twist::rt::Run([] {
    TwistedExample();
  });

  return 0;
}
