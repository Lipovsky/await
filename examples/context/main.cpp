#include <await/tasks/exe/thread_pool.hpp>

#include <carry/empty.hpp>
#include <carry/new.hpp>
#include <carry/wrap.hpp>

#include <await/tasks/curr/context.hpp>

#include <await/futures/make/just.hpp>

#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/apply.hpp>

#include <await/await.hpp>

#include <iostream>

using namespace await;

///////////////////////////////////////////////////////////////////////

// Контекст – иммутабельный словарь из строковых ключей в гетерогенные значения

// Типичное применение контекста - трассировка RPC

///////////////////////////////////////////////////////////////////////

void ConstructorsExample() {
  std::cout << "Basics Example" << std::endl;

  {
    auto empty = carry::Empty();

    [[maybe_unused]] auto maybe = empty.TryGet<int64_t>("key");
    assert(!maybe.has_value());
  }

  auto ctx1 = carry::New()
                  .SetString("key1", "value1")
                  .SetInt64("key2", 123)
                  .Done();

  auto ctx2 = carry::Wrap(ctx1)
                  .SetString("key1", "value2")  // Перезаписываем ключ
                  .SetString("key3", "value3")  // Устанавливаем новый ключ
                  .Done();

  std::cout << "ctx1: "
            << "key1 -> " << ctx1.Get<std::string>("key1") << ", "
            << "key2 -> " << ctx1.Get<int64_t>("key2") << std::endl;

  std::cout << "ctx2: "
            << "key1 -> " << ctx2.Get<std::string>("key1") << ", "
            << "key2 -> " << ctx2.Get<int64_t>("key2") << ", "
            << "key3 -> " << ctx2.Get<std::string>("key3") << std::endl;

  std::cout << std::endl;
}

///////////////////////////////////////////////////////////////////////

void CurrentExample() {
  std::cout << "Current Example" << std::endl;

  tasks::ThreadPool pool{4};

  // Конструируем новый контекст
  auto ctx = carry::New()
                 .SetString("request_id", "XYZ-123")
                 .Done();

  // Планируем задачу в пул потоков, задаем ей контекст
  auto f = futures::Just() |
           futures::Via(pool) |
           futures::With(ctx) |
           futures::Apply([]() {
             // Обращаемся к текущему контексту
             auto ctx = tasks::curr::Context();

             std::cout << "request_id = "
                       << ctx.Get<std::string>("request_id")
                       << std::endl;
           });

  // Запускаем задачу и блокируем поток до ее завершения
  Await(std::move(f));

  pool.Stop();

  std::cout << std::endl;
}

///////////////////////////////////////////////////////////////////////

void PropagationExample() {
  std::cout << "Propagation Example" << std::endl;

  tasks::ThreadPool pool{4};

  auto ctx = carry::New()
                 .SetString("request_id", "XYZ-123")
                 .Done();

  // Начинаем цепочку задач
  auto f = futures::Just() |
           futures::Via(pool) |
           futures::With(ctx) |
           futures::Apply([]() {
             auto ctx = tasks::curr::Context();

             std::cout << "f / request_id = "
                       << ctx.Get<std::string>("request_id")
                       << std::endl;
           });

  // Продолжаем цепочку задач
  // Каждая последующая задача автоматически наследует контекст предшествующей
  auto g = std::move(f) | futures::Apply([]() {
    auto ctx = tasks::curr::Context();

    std::cout << "g / request_id = "
              << ctx.Get<std::string>("request_id")
              << std::endl;
  });

  // Блокируем поток до завершения цепочки задач
  Await(std::move(g));

  pool.Stop();

  std::cout << std::endl;
}

///////////////////////////////////////////////////////////////////////

int main() {
  ConstructorsExample();
  CurrentExample();
  PropagationExample();
  return 0;
}
